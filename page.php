<?php 
    include ('header.php');

    $pageLink = $_GET['url'];
    $pageQRY  = "SELECT cat_id, cat_name, cat_subdesc, cat_desc FROM port_category WHERE cat_permalink = :catLink";
    $page     = $pdo->prepare($pageQRY);
    $page->bindValue(":catLink", $pageLink, PDO::PARAM_STR);
    $page->execute();

    if ($page->rowCount() < 1) :
        http_response_code(404);
        include("404.php");
        die();
    else :
    $page     = $page->fetchAll(PDO::FETCH_ASSOC);
    $pageId     = $page[0]['cat_id'];
    $pageName   = $page[0]['cat_name'];
    if ($page[0]['cat_subdesc'] == "") {
        $pageDesc = $page[0]['cat_desc'];
    } else {
        $pageDesc    = $page[0]['cat_subdesc'];
    }

    $postQRY  = "SELECT port_post.post_head, port_post.post_permalink, port_category.cat_name, port_category.cat_permalink, port_pimg.img_head1, port_pimg.img_thumb FROM port_post JOIN port_category ON port_post.cat_id = port_category.cat_id JOIN port_pimg ON port_post.post_id = port_pimg.post_id WHERE port_post.cat_id = :catid AND port_post.status = :status ORDER BY port_post.create_date DESC";
    $post     = $pdo->prepare($postQRY);
    $post->bindValue(":catid", $pageId, PDO::PARAM_STR);
    $post->bindValue(":status", "publish", PDO::PARAM_STR);
    $post->execute();
    $postCount = $post->rowCount();
    $post      = $post->fetchAll(PDO::FETCH_ASSOC);

?>

    <main class="main home-flush">

      <!-- Post Header
      ================================================== -->
      <div class="home-constraint">
        <header class="home-header">
          <h1 class="h-header-wrapper page__heading"><?php echo $pageName; ?></h1>
          <p class="h-header-wrapper page__subheading"><?php echo $pageDesc; ?></p>
        </header>
      </div>
      <!-- /.post header -->

      <div class="home-constraint">
        <section class="h-content-wrapper h-content page-bg-color">

        <?php  if ($postCount != 0 AND $postCount <= 5) : ?>
            <div class="page-item__grid-1">
                <a href="<?php echo $post[0]['cat_permalink'] ?>/<?php echo $post[0]['post_permalink']; ?>" class="page-item__link">
                    <header class="page-item__info">
                        <h2 class="page-item__title"><?php echo $post[0]['post_head']; ?></h2>
                    </header>
                    <picture>
                        <source srcset="img/pst/<?php echo $post[0]['img_head1'] ?>?filemtime(img/pst/<?php echo $post[0]['img_head1'] ?>)" media="(min-width: 568px)">
                        <source srcset="img/pst/<?php echo $post[0]['img_thumb'] ?>?filemtime(img/pst/<?php echo $post[0]['img_thumb'] ?>)">
                        <img srcset src = "img/pst/<?php echo $post[0]['img_head1'] ?>?filemtime(img/pst/<?php echo $post[0]['img_head1'] ?>)">
                    </picture>
                </a>
            </div>

            <?php for ($i=1; $i < $postCount; $i++) : ?> 
                <div class="page-item__grid-2">
                    <a href="<?php echo $post[$i]['cat_permalink'] ?>/<?php echo $post[$i]['post_permalink']; ?>" class="page-item__link">
                        <header class="page-item__info">
                            <h2 class="page-item__title"><?php echo $post[$i]['post_head']; ?></h2>
                        </header>
                        <picture>
                            <img srcset src = "img/pst/<?php echo $post[$i]['img_thumb'] ?>?filemtime(img/pst/<?php echo $post[$i]['img_thumb'] ?>)">
                        </picture>
                    </a>
                </div>
                
            <?php endfor; ?>
        <?php elseif ($postCount != 0 AND $postCount > 5) : ?>
            <div class="page-item__grid-1">
                <a href="<?php echo $post[0]['cat_permalink'] ?>/<?php echo $post[0]['post_permalink']; ?>" class="page-item__link">
                    <header class="page-item__info">
                        <h2 class="page-item__title"><?php echo $post[0]['post_head']; ?></h2>
                    </header>
                    <picture>
                        <source srcset="img/pst/<?php echo $post[0]['img_head1'] ?>?filemtime(img/pst/<?php echo $post[0]['img_head1'] ?>)" media="(min-width: 568px)">
                        <source srcset="img/pst/<?php echo $post[0]['img_thumb'] ?>?filemtime(img/pst/<?php echo $post[0]['img_thumb'] ?>)">
                        <img srcset src = "img/pst/<?php echo $post[0]['img_head1'] ?>?filemtime(img/pst/<?php echo $post[0]['img_head1'] ?>)">
                    </picture>
                </a>
            </div>

            <?php for ($i=1; $i < 5; $i++) : ?> 
                <div class="page-item__grid-2">
                    <a href="<?php echo $post[$i]['cat_permalink'] ?>/<?php echo $post[$i]['post_permalink']; ?>" class="page-item__link">
                        <header class="page-item__info">
                            <h2 class="page-item__title"><?php echo $post[$i]['post_head']; ?></h2>
                        </header>
                        <picture>
                            <img srcset src = "img/pst/<?php echo $post[$i]['img_thumb'] ?>?filemtime(img/pst/<?php echo $post[$i]['img_thumb'] ?>)">
                        </picture>
                    </a>
                </div>
            <?php endfor; ?>

            <?php for ($i=5; $i < $postCount; $i++) : ?>
                <div class="page-item__grid-3">
                    <a href="<?php echo $post[$i]['cat_permalink'] ?>/<?php echo $post[$i]['post_permalink']; ?>" class="page-item__link">
                        <header class="page-item__info">
                            <h2 class="page-item__title"><?php echo $post[$i]['post_head']; ?></h2>
                        </header>
                        <picture>
                            <img srcset src = "img/pst/<?php echo $post[$i]['img_thumb'] ?>?filemtime(img/pst/<?php echo $post[$i]['img_thumb'] ?>)">
                        </picture>
                    </a>
                </div>
            <?php endfor; ?>
        <?php endif; ?>

        </section>
      </div>

    </main>

<?php 
    include ('footer.php');
    endif;
?>

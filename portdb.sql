-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 22, 2018 at 11:02 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `portdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `port_category`
--

CREATE TABLE `port_category` (
  `cat_id` int(255) NOT NULL,
  `cat_name` varchar(255) NOT NULL,
  `cat_permalink` varchar(255) NOT NULL,
  `cat_desc` text,
  `cat_subdesc` text,
  `cat_img` varchar(255) DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'publish'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `port_pimg`
--

CREATE TABLE `port_pimg` (
  `img_id` int(255) NOT NULL,
  `post_id` int(255) DEFAULT NULL,
  `img_head1` varchar(255) NOT NULL,
  `img_head2` varchar(255) NOT NULL,
  `img_head3` varchar(255) NOT NULL,
  `img_thumb` varchar(255) NOT NULL,
  `img_post` varchar(255) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `modified_by` varchar(255) NOT NULL,
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `modified_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `port_pimg`
--

INSERT INTO `port_pimg` (`img_id`, `post_id`, `img_head1`, `img_head2`, `img_head3`, `img_thumb`, `img_post`, `create_by`, `modified_by`, `create_date`, `modified_date`) VALUES
(1, NULL, 'kodok-terbang-h.jpg', 'kodok-terbang-h-480x600.jpg', 'kodok-terbang-h-768x576.jpg', 'kodok-terbang-thumb.jpg', 'kodok-terbang-p.jpg', '0', 'root-admin', '2018-10-19 07:06:18.921391', '2018-10-21 05:00:02.977718'),
(2, NULL, 'slime-monster-1-h.jpg', 'slime-monster-1-h-480x600.jpg', 'slime-monster-1-h-768x576.jpg', 'slime-monster-1-thumb.jpg', 'slime-monster-1-p.jpg', '0', 'admin', '2018-10-19 07:15:08.138813', '2018-10-22 08:47:47.721486'),
(3, NULL, 'kodok-terbang-1-h.jpg', 'kodok-terbang-1-h-480x600.jpg', 'kodok-terbang-1-h-768x576.jpg', 'kodok-terbang-1-thumb.jpg', 'kodok-terbang-1-p.jpg', 'root-admin', 'root-admin', '2018-10-19 07:17:21.237795', '2018-10-19 07:17:21.237795'),
(4, NULL, 'kodok-terbang-1-h.jpg', 'kodok-terbang-1-h-480x600.jpg', 'kodok-terbang-1-h-768x576.jpg', 'kodok-terbang-1-thumb.jpg', 'kodok-terbang-1-p.jpg', 'root-admin', 'root-admin', '2018-10-19 07:18:16.177555', '2018-10-19 07:18:16.177555'),
(5, NULL, 'robot-2-h.jpg', 'robot-2-h-480x600.jpg', 'robot-2-h-768x576.jpg', 'robot-2-thumb.jpg', 'robot-2-p.jpg', 'root-admin', 'admin', '2018-10-19 20:30:22.270663', '2018-10-22 08:48:40.467503'),
(6, NULL, 'kodok-terbang-3-h.jpg', 'kodok-terbang-3-h-480x600.jpg', 'kodok-terbang-3-h-768x576.jpg', 'kodok-terbang-3-thumb.jpg', 'kodok-terbang-3-p.jpg', 'root-admin', 'admin', '2018-10-19 20:31:03.465019', '2018-10-22 07:54:57.802170'),
(7, NULL, 'sheep-turbo-h.jpg', 'sheep-turbo-h-480x600.jpg', 'sheep-turbo-h-768x576.jpg', 'sheep-turbo-thumb.jpg', 'sheep-turbo-p.jpg', 'root-admin', 'admin', '2018-10-19 20:31:13.390586', '2018-10-22 08:48:22.189457'),
(8, NULL, 'the-tiniest-shark-h.jpg', 'the-tiniest-shark-h-480x600.jpg', 'the-tiniest-shark-h-768x576.jpg', 'the-tiniest-shark-thumb.jpg', 'the-tiniest-shark-p.jpg', 'root-admin', 'admin', '2018-10-19 20:32:52.404250', '2018-10-22 08:45:37.697697'),
(9, NULL, 'test-pack-h.jpg', 'test-pack-h-480x600.jpg', 'test-pack-h-768x576.jpg', 'test-pack-thumb.jpg', 'test-pack-p.jpg', 'root-admin', 'root-admin', '2018-10-19 20:36:31.899804', '2018-10-19 20:36:31.899804'),
(10, NULL, 'haypi-dungeon-h.jpg', 'haypi-dungeon-h-480x600.jpg', 'haypi-dungeon-h-768x576.jpg', 'haypi-dungeon-thumb.jpg', 'haypi-dungeon-p.jpg', 'admin', 'admin', '2018-10-22 07:39:06.084797', '2018-10-22 07:39:06.084797');

-- --------------------------------------------------------

--
-- Table structure for table `port_post`
--

CREATE TABLE `port_post` (
  `post_id` int(255) NOT NULL,
  `cat_id` int(255) NOT NULL,
  `post_head` varchar(255) NOT NULL,
  `post_permalink` varchar(255) NOT NULL,
  `post_client` varchar(255) DEFAULT NULL,
  `post_project` varchar(255) DEFAULT NULL,
  `post_location` varchar(255) DEFAULT NULL,
  `post_desc` varchar(255) DEFAULT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'publish',
  `create_by` varchar(255) NOT NULL,
  `modified_by` varchar(255) NOT NULL,
  `create_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `modified_date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `port_user`
--

CREATE TABLE `port_user` (
  `user_id` int(255) NOT NULL,
  `user_username` varchar(255) NOT NULL,
  `user_fullname` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'register'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `webinfo`
--

CREATE TABLE `webinfo` (
  `id` int(11) NOT NULL,
  `home-header` varchar(255) DEFAULT NULL,
  `home-desc` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `webinfo`
--

INSERT INTO `webinfo` (`id`, `home-header`, `home-desc`) VALUES
(2018, 'portofolio', 'There is an expansive collection of work within the portfolio, please select a category to refine your selection.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `port_category`
--
ALTER TABLE `port_category`
  ADD PRIMARY KEY (`cat_id`),
  ADD UNIQUE KEY `cat_name` (`cat_name`),
  ADD UNIQUE KEY `cat_permalink` (`cat_permalink`);

--
-- Indexes for table `port_pimg`
--
ALTER TABLE `port_pimg`
  ADD PRIMARY KEY (`img_id`),
  ADD KEY `postIdKey` (`post_id`);

--
-- Indexes for table `port_post`
--
ALTER TABLE `port_post`
  ADD PRIMARY KEY (`post_id`),
  ADD UNIQUE KEY `post_permalink` (`post_permalink`),
  ADD KEY `catIdKey` (`cat_id`);

--
-- Indexes for table `port_user`
--
ALTER TABLE `port_user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_username` (`user_username`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- Indexes for table `webinfo`
--
ALTER TABLE `webinfo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `port_category`
--
ALTER TABLE `port_category`
  MODIFY `cat_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `port_pimg`
--
ALTER TABLE `port_pimg`
  MODIFY `img_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `port_post`
--
ALTER TABLE `port_post`
  MODIFY `post_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `port_user`
--
ALTER TABLE `port_user`
  MODIFY `user_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `webinfo`
--
ALTER TABLE `webinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2019;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `port_pimg`
--
ALTER TABLE `port_pimg`
  ADD CONSTRAINT `postIdKey` FOREIGN KEY (`post_id`) REFERENCES `port_post` (`post_id`) ON DELETE SET NULL;

--
-- Constraints for table `port_post`
--
ALTER TABLE `port_post`
  ADD CONSTRAINT `catIdKey` FOREIGN KEY (`cat_id`) REFERENCES `port_category` (`cat_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

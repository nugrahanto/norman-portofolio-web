<?php 
  include ('header.php');

  $stmt = $pdo->prepare('SELECT * FROM webinfo');
  $stmt->execute();
  $web = $stmt->fetchAll(PDO::FETCH_ASSOC);
  $webHead = $web[0]['home-header'];
  $webDesc = $web[0]['home-desc'];
?>

    <main class="main home-flush">

      <!-- Post Header
      ================================================== -->
      <div class="home-constraint">
        <header class="home-header">
          <h1 class="h-header-wrapper h-header__heading"><?php echo $webHead; ?></h1>
          <p class="h-header-wrapper h-header__subheading"><?php echo $webDesc; ?></p>
        </header>
      </div>
      <!-- /.post header -->

      <div class="home-constraint">
        <section class="h-content-wrapper h-content">

        <?php 
          //fetch data
          foreach ($catFetch as $data) : 
        ?>

          <article class="h-categories__item">
            <a href="<?php echo $data['cat_permalink'] ?>" class="h-categories__item-link" title="<?php echo $data['cat_name']; ?>">
              <div class="h-categories__wrapper">
                <div class="h-content-wrapper h-content h-categories__item-info">
                  <h2 class="h-categories__item-heading"><?php echo $data['cat_name']; ?></h2>
                  <div class="h-categories__item-detail"><p><?php echo $data['cat_desc']; ?></p></div>
                  <span class="h-categories__item-link-text">View the Portfolio</span>
                </div>
              </div><!-- /.h-categories__wrapper -->
              <picture class="h-categories__picture">
                <source srcset="img/cat/<?php echo $data['cat_img']; ?>?filemtime(img/cat/<?php echo $data['cat_img'] ?>)" media="(min-width: 48em)">
                <img class="h-categories__item-image" srcset="img/cat/<?php echo $data['cat_img']; ?>?filemtime(img/cat/<?php echo $data['cat_img'] ?>)" 
              </picture>
            </a><!-- /.h-categories__item-link -->
          </article>

        <?php endforeach; ?>

        </section>
      </div>

      <section class="more-post-wrapper">
        <div class="more-post-header">
          <h1 class="heading-width more-post-header-heading">Latest Projects</h1>
        </div>
        <div class="container">

          <!-- START THE FEATURETTES -->
          <div class="row featurette">

            <?php 
              $postStmt = "SELECT port_post.post_head, port_post.post_permalink, port_category.cat_name, port_category.cat_permalink, port_pimg.img_head1, port_pimg.img_thumb FROM port_post JOIN port_category ON port_post.cat_id = port_category.cat_id JOIN port_pimg ON port_post.post_id = port_pimg.post_id WHERE port_post.status = 'publish' ORDER BY port_post.create_date DESC LIMIT 9";
              $postStmt = $pdo->prepare($postStmt);
              $postStmt->execute();
              $getData = $postStmt->fetchAll(PDO::FETCH_ASSOC);
              $getCount = $postStmt->rowCount();
            ?>

            <?php 
              if ($getCount <= 3):
                for ($i=0; $i < $getCount; $i++) :
            ?>
              <div class="port-item__grid-1">
                <div class="more-post-content">
                  <a href="<?php echo $getData[$i]['cat_permalink'] ?>/<?php echo $getData[$i]['post_permalink']; ?>" class="port-item__thumbnail">
                    <div class="port-item__info">
                      <header>
                        <h2 class="port-item__title"><?php echo $getData[$i]['post_head']; ?></h2>
                      </header>
                      <p class="port-item__tags"><?php echo $getData[$i]['cat_name']; ?></p>
                    </div>
                    <img src="img/pst/<?php echo $getData[$i]['img_head1'] ?>?filemtime(img/pst/<?php echo $getData[$i]['img_head1'] ?>)">
                  </a>
                </div>
              </div><!-- /.port-item__grid-1 -->
            <?php endfor; ?>

            <?php 
              elseif ($getCount <= 6):
                for ($i=0; $i < $getCount; $i++) :
            ?>
              <div class="port-item__grid-2">
                <div class="more-post-content">
                  <a href="<?php echo $getData[$i]['cat_permalink'] ?>/<?php echo $getData[$i]['post_permalink'] ?>" class="port-item__thumbnail">
                    <div class="port-item__info">
                      <header>
                        <h2 class="port-item__title"><?php echo $getData[$i]['post_head']; ?></h2>
                      </header>
                      <p class="port-item__tags"><?php echo $getData[$i]['cat_name']; ?></p>
                    </div>
                    <img src="img/pst/<?php echo $getData[$i]['img_thumb'] ?>?filemtime(img/pst/<?php echo $getData[$i]['img_thumb'] ?>)">
                  </a>
                </div>
              </div><!-- /.port-item__grid-2 -->
            <?php endfor; ?>

            <?php elseif ($getCount <= 8): ?>
              <div class="port-item__grid-2">
                <div class="more-post-content">
                  <a href="<?php echo $getData[0]['cat_permalink'] ?>/<?php echo $getData[0]['post_permalink'] ?>" class="port-item__thumbnail">
                    <div class="port-item__info">
                      <header>
                        <h2 class="port-item__title"><?php echo $getData[0]['post_head']; ?></h2>
                      </header>
                      <p class="port-item__tags"><?php echo $getData[0]['cat_name']; ?></p>
                    </div>
                    <img src="img/pst/<?php echo $getData[0]['img_thumb'] ?>?filemtime(img/pst/<?php echo $getData[0]['img_thumb'] ?>)">
                  </a>
                </div>
              </div><!-- /.port-item__grid-2 -->
              <div class="port-item__grid-2">
                <div class="more-post-content">
                  <a href="<?php echo $getData[1]['cat_permalink'] ?>/<?php echo $getData[1]['post_permalink'] ?>" class="port-item__thumbnail">
                    <div class="port-item__info">
                      <header>
                        <h2 class="port-item__title"><?php echo $getData[1]['post_head']; ?></h2>
                      </header>
                      <p class="port-item__tags"><?php echo $getData[1]['cat_name']; ?></p>
                    </div>
                    <img src="img/pst/<?php echo $getData[1]['img_thumb'] ?>?filemtime(img/pst/<?php echo $getData[1]['img_thumb'] ?>)">
                  </a>
                </div>
              </div><!-- /.port-item__grid-2 -->
              <?php for ($i=2; $i < $getCount; $i++) : ?>
              <div class="port-item__grid-3">
                <div class="more-post-content">
                  <a href="<?php echo $getData[$i]['cat_permalink'] ?>/<?php echo $getData[$i]['post_permalink'] ?>" class="port-item__thumbnail">
                    <div class="port-item__info">
                      <header>
                        <h2 class="port-item__title"><?php echo $getData[$i]['post_head']; ?></h2>
                      </header>
                      <p class="port-item__tags"><?php echo $getData[$i]['cat_name']; ?></p>
                    </div>
                    <img src="img/pst/<?php echo $getData[$i]['img_thumb'] ?>?filemtime(img/pst/<?php echo $getData[$i]['img_thumb'] ?>)">
                  </a>
                </div>
              </div><!-- /.port-item__grid-3 -->
            <?php endfor; ?>

            <?php elseif ($getCount <= 9): ?>
              <?php for ($i=0; $i < 2 ; $i++) : ?>
                <div class="port-item__grid-2">
                  <div class="more-post-content">
                    <a href="<?php echo $getData[$i]['cat_permalink'] ?>/<?php echo $getData[$i]['post_permalink'] ?>" class="port-item__thumbnail">
                      <div class="port-item__info">
                        <header>
                          <h2 class="port-item__title"><?php echo $getData[$i]['post_head']; ?></h2>
                        </header>
                        <p class="port-item__tags"><?php echo $getData[$i]['cat_name']; ?></p>
                      </div>
                      <img src="img/pst/<?php echo $getData[$i]['img_thumb'] ?>?filemtime(img/pst/<?php echo $getData[$i]['img_thumb'] ?>)">
                    </a>
                  </div>
                </div><!-- /.port-item__grid-2 -->
              <?php endfor; ?>
              <?php for ($i=2; $i < 8 ; $i++) : ?>
                <div class="port-item__grid-3">
                  <div class="more-post-content">
                    <a href="<?php echo $getData[$i]['cat_permalink'] ?>/<?php echo $getData[$i]['post_permalink'] ?>" class="port-item__thumbnail">
                      <div class="port-item__info">
                        <header>
                          <h2 class="port-item__title"><?php echo $getData[$i]['post_head']; ?></h2>
                        </header>
                        <p class="port-item__tags"><?php echo $getData[$i]['cat_name']; ?></p>
                      </div>
                      <img src="img/pst/<?php echo $getData[$i]['img_thumb'] ?>?filemtime(img/pst/<?php echo $getData[$i]['img_thumb'] ?>)">
                    </a>
                  </div>
                </div><!-- /.port-item__grid-3 -->
              <?php endfor; ?>
              <div class="port-item__grid-1">
                <div class="more-post-content">
                  <a href="<?php echo $getData[8]['cat_permalink'] ?>/<?php echo $getData[8]['post_permalink']; ?>" class="port-item__thumbnail">
                    <div class="port-item__info">
                      <header>
                        <h2 class="port-item__title"><?php echo $getData[8]['post_head']; ?></h2>
                      </header>
                      <p class="port-item__tags"><?php echo $getData[8]['cat_name']; ?></p>
                    </div>
                    <img src="img/pst/<?php echo $getData[8]['img_head1'] ?>?filemtime(img/pst/<?php echo $getData[8]['img_head1'] ?>)">
                  </a>
                </div>
              </div><!-- /.port-item__grid-1 -->
            <?php endif; ?>
            
          </div><!-- /.row featurette -->
          <!-- /END THE FEATURETTES -->

        </div><!-- /.container -->
      </section>

    </main>

<?php include ('footer.php'); ?>

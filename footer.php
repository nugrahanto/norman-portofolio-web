
    <!-- FOOTER -->
    <footer>
      <div class="footer-wrapper">
        <div class="container">
          <div class="footer-nav">
            <hr>
            <dd><a href="#">Home</a></dd>
            <dd><a href="">Contact</a></dd>
            <dd><a href="">Login</a></dd>
          </div>
          <div class="footer-cat">
            <hr>
            <?php  foreach ($catFetch as $footData) : ?>
              <dd><a href="<?php echo $footData['cat_permalink'] ?>"><?php echo $footData['cat_name'] ?></a></dd>
            <?php endforeach; ?>
          </div>
        </div>
        <div class="col-md-12 footer-copyright">
          <p>&copy; 2018, <a href="/">normandesign.com</a> </p>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>

    <?php 
    if (isset($_GET['alert'])) {
        $loginAlert = $_GET['alert'];
        $loginResult = $_GET['result'];

        if ($loginAlert == 'login') { 
    ?>

          <script type="text/javascript">
            $(document).ready(function(){
              $(".dropdown-toggle").dropdown("toggle");
            });
          </script>
         
    <?php 
        }
    }
    ?>

  </body>
</html>

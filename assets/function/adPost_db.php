<?php 
	$root = BASE_URL;

	if(isset($_POST['post-article'])) {
		// include ImageManipulator class
		require_once('ImageManipulator.php');

		$user		= $_SESSION["access-login"];
		$head 		= $_POST['header'];
		$category	= $_POST['category'];
		// change post_name remove space to "-" for permalink use
		$TmpLink	= fungsi_name($head);
		$client		= $_POST['client'];
		$project	= $_POST['project'];
		$location	= $_POST['location'];
		$detail		= $_POST['detail'];

		// upload directory destination
		$uploadTmp 	= "../img/tmp/";
		$uploadDir 	= "../img/pst/";

		// post permalink set
		$postlinkQRY = "SELECT post_permalink FROM port_post WHERE post_permalink = :permalink OR post_permalink REGEXP :linkclause ORDER BY LENGTH(post_permalink) DESC LIMIT 1";
		$postLink = $pdo->prepare($postlinkQRY);
		$postLink->bindValue(":permalink", $TmpLink, PDO::PARAM_STR);
		$postLink->bindValue(":linkclause", $TmpLink."-[0-9]*", PDO::PARAM_STR);
		$postLink->execute();
		if ($postLink->rowCount() > 0) {
			$result = $postLink->fetchAll(PDO::FETCH_ASSOC);
			$result = $result[0]['post_permalink'];
			$count = strrchr($result, "-");
			$count = str_replace("-", "", $count);
			$count += 1;
			$permalink = $TmpLink . "-" . $count;
		} else {
			$permalink = $TmpLink;
		}
		
		// check head post image
		if ($_FILES['HimgInp']['error']  > 0 ) {
			$fileExtension = ".jpg";

			$Himg_1 = copy($uploadTmp . "no-image-h.jpg", $uploadTmp . $permalink . '-h' . $fileExtension);
			$Himg_2 = copy($uploadTmp . "no-image-h-480x600.jpg", $uploadTmp . $permalink . '-h-480x600' . $fileExtension);
			$Himg_3 = copy($uploadTmp . "no-image-h-768x576.jpg", $uploadTmp . $permalink . '-h-768x576' . $fileExtension);

			$Himg_1 = $permalink . '-h' . $fileExtension;
			$Himg_2 = $permalink . '-h-480x600' . $fileExtension;
			$Himg_3 = $permalink . '-h-768x576' . $fileExtension;
		} else {
			// array of valid extensions
			$validExtensions = array('.jpg', '.jpeg', '.gif', '.png');
			// get extension of the uploaded file
			$fileExtension = strrchr($_FILES['HimgInp']['name'], ".");

			if (in_array($fileExtension, $validExtensions)) {

				// initialization image
				$Himg_2 = new ImageManipulator($_FILES['HimgInp']['tmp_name']);
				$Himg_3 = new ImageManipulator($_FILES['HimgInp']['tmp_name']);

				// set image 
				$width  = $Himg_2->getWidth();
				$height = $Himg_2->getHeight();
				$centreX = round($width / 2);
				$centreY = round($height / 2);


				//----------------------------------------------//
				//---------------480x600-SECTION----------------//
				//----------------------------------------------//

				// scale image to 480x600 from max height
				$img2_Width = round(($height * 0.8) / 2); //scaling img width from height
				$img2_Height = round($height / 2);

				// positioning img 480x600 to crop
				$img2_x1 = $centreX - $img2_Width; 
				$img2_y1 = $centreY - $img2_Height; 
				$img2_x2 = $centreX + $img2_Width; 
				$img2_y2 = $centreY + $img2_Height;

				// cropping then resize image to 480x600 from center image
				$Himg_2->crop($img2_x1, $img2_y1, $img2_x2, $img2_y2);
				$Himg_2->resample(480, 600);


				//----------------------------------------------//
				//---------------768x576-SECTION----------------//
				//----------------------------------------------//

				// scale image to 768x576 from max height
				$img3_Width = round(((768 / 576) * $height) / 2); //scaling img width from height
				$img3_Height = round($height / 2);

				// positioning img 768x576 to crop
				$img3_x1 = $centreX - $img3_Width; 
				$img3_y1 = $centreY - $img3_Height; 
				$img3_x2 = $centreX + $img3_Width; 
				$img3_y2 = $centreY + $img3_Height;

				// cropping then resize image to 768x576 from center image
				$Himg_3->crop($img3_x1, $img3_y1, $img3_x2, $img3_y2);
				$Himg_3->resample(768, 576);


				//----------------------------------------------//
				//----------------UPLOAD-SECTION----------------//
				//----------------------------------------------//

				// saving file to uploads folder
				$Himg_1 = move_uploaded_file($_FILES["HimgInp"]["tmp_name"], $uploadTmp . $permalink . '-h' . $fileExtension);
				if ($Himg_1) {
					if (!$Himg_2->save($uploadTmp . $permalink . '-h-480x600' . $fileExtension)) {
						if (!$Himg_3->save($uploadTmp . $permalink . '-h-768x576' . $fileExtension)) {
							$Himg_1 = $permalink . '-h' . $fileExtension;
							$Himg_2 = $permalink . '-h-480x600' . $fileExtension;
							$Himg_3 = $permalink . '-h-768x576' . $fileExtension;
						} else {
							header("location: $root/404.php");
						}
					} else {
						header("location: $root/404.php");
					}
				} else {
					header("location: $root/404.php");
				}
				
			}
		}

		// check thumbnail post image
		if ($_FILES['FimgInp']['error']  > 0 ) {
			$fileExtension = ".jpg";
			$Fimg = copy($uploadTmp . "no-image-thumb.jpg", $uploadTmp . $permalink . '-thumb' . $fileExtension);
			$Fimg = $permalink . '-thumb' . $fileExtension;
		} else {
			// array of valid extensions
			$validExtensions = array('.jpg', '.jpeg', '.gif', '.png');
			// get extension of the uploaded file
			$fileExtension = strrchr($_FILES['FimgInp']['name'], ".");

			if (in_array($fileExtension, $validExtensions)) {
				// initialization image
				$Fimg = new ImageManipulator($_FILES['FimgInp']['tmp_name']);

				// set image 
				$width  = $Fimg->getWidth();
				$height = $Fimg->getHeight();
				$centreX = round($width / 2);
				$centreY = round($height / 2);

				// scale image to 720x405 from max height
				$img_Width = round(((720 / 405) * $height) / 2); //scaling img width from height
				$img_Height = round($height / 2);

				// positioning img 768x576 to crop
				$imgX1 = $centreX - $img_Width; 
				$imgY1 = $centreY - $img_Height; 
				$imgX2 = $centreX + $img_Width; 
				$imgY2 = $centreY + $img_Height;

				// cropping then resize image to 768x576 from center image
				$Fimg->crop($imgX1, $imgY1, $imgX2, $imgY2);
				$Fimg->resample(720, 405);

				if (!$Fimg->save($uploadTmp . $permalink . '-thumb' . $fileExtension)) {
					$Fimg = $permalink . '-thumb' . $fileExtension;
				} else {
					header("location: $root/404.php");
				}
			}
		}

		// check post image
		if ($_FILES['PimgInp']['error']  > 0 ) {
			$fileExtension = ".jpg";
			$Pimg = copy($uploadTmp . "no-image-p.jpg", $uploadTmp . $permalink . '-p' . $fileExtension);
			$Pimg = $permalink . '-p' . $fileExtension;
		} else {
			$fileExtension = strrchr($_FILES['PimgInp']['name'], ".");
			$Pimg = move_uploaded_file($_FILES["PimgInp"]["tmp_name"], $uploadTmp . $permalink . '-p' . $fileExtension);
			if (!$Pimg) {
				header("location: $root/404.php");
			} else {
				$Pimg = $permalink . '-p' . $fileExtension;
			}
		}

		// begin transaction
		$pdo->beginTransaction();

		try {
			// get id of category 
			$getcatIdQry = "SELECT cat_id FROM port_category WHERE cat_permalink=:permalink";
			$catStmt = $pdo->prepare($getcatIdQry);
			$catStmt->bindValue(":permalink", $category, PDO::PARAM_STR);
			$catStmt->execute();
			$catId = $catStmt->fetchAll(PDO::FETCH_ASSOC);
			$catId = $catId[0]['cat_id'];

			// insert data input to post table
			$postQry = "INSERT INTO port_post (cat_id,post_head,post_permalink,post_client,post_project,post_location,post_desc,create_by,modified_by) VALUES (:catid,:head,:permalink,:client,:project,:location,:detail,:createby,:modifiedby)";
			$postStmt = $pdo->prepare($postQry);
			$postStmt->bindValue(":catid", $catId, PDO::PARAM_STR);
			$postStmt->bindValue(":head", $head, PDO::PARAM_STR);
			$postStmt->bindValue(":permalink", $permalink, PDO::PARAM_STR);
			$postStmt->bindValue(":client", $client, PDO::PARAM_STR);
			$postStmt->bindValue(":project", $project, PDO::PARAM_STR);
			$postStmt->bindValue(":location", $location, PDO::PARAM_STR);
			$postStmt->bindValue(":detail", $detail, PDO::PARAM_STR);
			$postStmt->bindValue(":createby", $user, PDO::PARAM_STR);
			$postStmt->bindValue(":modifiedby", $user, PDO::PARAM_STR);
			$postStmt->execute();

			// get last inserted id post
			$lastInsertID = $pdo->lastInsertId();

			// insert img data to table
			$imgQry = "INSERT INTO port_pimg (post_id,img_head1,img_head2,img_head3,img_thumb,img_post,create_by,modified_by) VALUES (:postid,:headimg1,:headimg2,:headimg3,:thumbimg,:postimg,:createby,:modifiedby)";
			$imgStmt = $pdo->prepare($imgQry);
			$imgStmt->bindValue(":postid", $lastInsertID, PDO::PARAM_STR);
			$imgStmt->bindValue(":headimg1", $Himg_1, PDO::PARAM_STR);
			$imgStmt->bindValue(":headimg2", $Himg_2, PDO::PARAM_STR);
			$imgStmt->bindValue(":headimg3", $Himg_3, PDO::PARAM_STR);
			$imgStmt->bindValue(":thumbimg", $Fimg, PDO::PARAM_STR);
			$imgStmt->bindValue(":postimg", $Pimg, PDO::PARAM_STR);
			$imgStmt->bindValue(":createby", $user, PDO::PARAM_STR);
			$imgStmt->bindValue(":modifiedby", $user, PDO::PARAM_STR);
			$imgStmt->execute();

			$uploadHimg1 = rename($uploadTmp . $Himg_1, $uploadDir . $Himg_1);
			if ($uploadHimg1) {
				$uploadHimg2 = rename($uploadTmp . $Himg_2, $uploadDir . $Himg_2);
				if ($uploadHimg2) {
					$uploadHimg3 = rename($uploadTmp . $Himg_3, $uploadDir . $Himg_3);
					if ($uploadHimg3) {
						$uploadFimg = rename($uploadTmp . $Fimg, $uploadDir . $Fimg);
						if ($uploadFimg) {
							$uploadPimg = rename($uploadTmp . $Pimg, $uploadDir . $Pimg);
							if ($uploadPimg) {
								// commit transaction
								$pdo->commit();
								header("location: $root/port-admin/$category");
							} else {
								$pdo->rollback();
								header("location: $root/404.php");
							}
						} else {
							$pdo->rollback();
							header("location: $root/404.php");
						}
					} else {
						$pdo->rollback();
						header("location: $root/404.php");
					}
				} else {
					$pdo->rollback();
					header("location: $root/404.php");
				}
			} else {
				$pdo->rollback();
				header("location: $root/404.php");
			}
		} 

		catch (PDOException $e) {
			$pdo->rollback();
			header("location: $root/404.php");
		}
	}

	if(isset($_POST['put-article'])) {
		// include ImageManipulator class
		require_once('ImageManipulator.php');

		$user			= $_SESSION["access-login"];
		$postId 		= $_POST['postId'];
		$category		= $_POST['category'];
		$head 			= $_POST['header'];
		$dbhead 		= $_POST['dbhead'];
		$permalink		= $_POST['permalink'];
		// change post_name remove space to "-" for permalink use
		//$TmpLink		= fungsi_name($head);
		$client			= $_POST['client'];
		$project		= $_POST['project'];
		$location		= $_POST['location'];
		$detail			= $_POST['detail'];
		$HimgInp_txt	= $_POST['Himg_value'];
		$PimgInp_txt	= $_POST['Pimg_value'];
		$FimgInp_txt	= $_POST['Fimg_value'];

		// upload directory destination
		$uploadTmp 	= "../img/tmp/";
		$uploadDir 	= "../img/pst/";


		// begin transaction
		$pdo->beginTransaction();

		try {
			if ($head == $dbhead) {
				$permalink = $permalink;
			} else {
				$TmpLink = fungsi_name($head);

				// post permalink set
				$postlinkQRY = "SELECT post_permalink FROM port_post WHERE post_permalink = :permalink OR post_permalink REGEXP :linkclause ORDER BY LENGTH(post_permalink) DESC LIMIT 1";
				$postLink = $pdo->prepare($postlinkQRY);
				$postLink->bindValue(":permalink", $TmpLink, PDO::PARAM_STR);
				$postLink->bindValue(":linkclause", $TmpLink."-[0-9]*", PDO::PARAM_STR);
				$postLink->execute();

				if ($postLink->rowCount() > 0) {
					$result = $postLink->fetchAll(PDO::FETCH_ASSOC);
					$result = $result[0]['post_permalink'];
					$count = strrchr($result, "-");
					$count = str_replace("-", "", $count);
					$count += 1;
					$permalink = $TmpLink . "-" . $count;
				} else {
					$permalink = $TmpLink;
				}
			}

			$postQry = "UPDATE port_post SET `post_head`=:head, `post_permalink`=:permalink, `post_client`=:client, `post_project`=:project, `post_location`=:location, `post_desc`=:desc, `modified_by`=:user WHERE `post_id`=:id";
			$postUpdate = $pdo->prepare($postQry);
			$postUpdate->bindValue(":head", $head, PDO::PARAM_STR);
			$postUpdate->bindValue(":permalink", $permalink, PDO::PARAM_STR);
			$postUpdate->bindValue(":client", $client, PDO::PARAM_STR);
			$postUpdate->bindValue(":project", $project, PDO::PARAM_STR);
			$postUpdate->bindValue(":location", $location, PDO::PARAM_STR);
			$postUpdate->bindValue(":desc", $detail, PDO::PARAM_STR);
			$postUpdate->bindValue(":id", $postId, PDO::PARAM_STR);
			$postUpdate->bindValue(":user", $user, PDO::PARAM_STR);
			$postUpdate->execute();

			$pimgGetQry = "SELECT * FROM port_pimg WHERE post_id = :id ";
			$pimgGET = $pdo->prepare($pimgGetQry);
			$pimgGET->bindValue(":id", $postId, PDO::PARAM_STR);
			$pimgGET->execute();
			$pimgGET = $pimgGET->fetchAll(PDO::FETCH_ASSOC);
			$imgH1 	 = $pimgGET[0]['img_head1'];
			$imgH2 	 = $pimgGET[0]['img_head2'];
			$imgH3 	 = $pimgGET[0]['img_head3'];
			$imgP 	 = $pimgGET[0]['img_post'];
			$imgF	 = $pimgGET[0]['img_thumb'];
			try {
				$moveH1 = copy($uploadDir . $imgH1, $uploadTmp . $imgH1);
				if (!$moveH1) {
					throw new Exception('Can not edit header 1 image');
				}
				$moveH2 = copy($uploadDir . $imgH2, $uploadTmp . $imgH2);
				if (!$moveH2) {
					throw new Exception('Can not edit header 1 image');
				}
				$moveH3 = copy($uploadDir . $imgH3, $uploadTmp . $imgH3);
				if (!$moveH3) {
					throw new Exception('Can not edit header 3 image');
				}
				$moveP = copy($uploadDir . $imgP, $uploadTmp . $imgP);
				if (!$moveP) {
					throw new Exception('Can not edit post image');
				}
				$moveF = copy($uploadDir . $imgF, $uploadTmp . $imgF);
				if (!$moveF) {
					throw new Exception('Can not edit thumbnail image');
				}
			} catch (Exception $e) {
				$pdo->rollback();
				http_response_code(404);
				include("../404.php");
				die();
			}

			if ($_FILES['HimgInp']['error']  > 0 ) {
				if ($HimgInp_txt == $imgH1) {
					$fileExtension = strrchr($imgH1, ".");
					rename($uploadTmp . $imgH1, $uploadTmp . $permalink . '-h' . $fileExtension);
					rename($uploadTmp . $imgH2, $uploadTmp . $permalink . '-h-480x600' . $fileExtension);
					rename($uploadTmp . $imgH3, $uploadTmp . $permalink . '-h-768x576' . $fileExtension);
					$Himg_1 = $permalink . '-h' . $fileExtension;
					$Himg_2 = $permalink . '-h-480x600' . $fileExtension;
					$Himg_3 = $permalink . '-h-768x576' . $fileExtension;
				} else if ($HimgInp_txt == "") {
					$fileExtension = ".jpg";

					$Himg_1 = copy($uploadTmp . "no-image-h.jpg", $uploadTmp . $permalink . '-h' . $fileExtension);
					$Himg_2 = copy($uploadTmp . "no-image-h-480x600.jpg", $uploadTmp . $permalink . '-h-480x600' . $fileExtension);
					$Himg_3 = copy($uploadTmp . "no-image-h-768x576.jpg", $uploadTmp . $permalink . '-h-768x576' . $fileExtension);

					$Himg_1 = $permalink . '-h' . $fileExtension;
					$Himg_2 = $permalink . '-h-480x600' . $fileExtension;
					$Himg_3 = $permalink . '-h-768x576' . $fileExtension;
				}
			} else {
				// array of valid extensions
				$validExtensions = array('.jpg', '.jpeg', '.gif', '.png');
				// get extension of the uploaded file
				$fileExtension = strrchr($_FILES['HimgInp']['name'], ".");

				if (in_array($fileExtension, $validExtensions)) {

					// initialization image
					$Himg_2 = new ImageManipulator($_FILES['HimgInp']['tmp_name']);
					$Himg_3 = new ImageManipulator($_FILES['HimgInp']['tmp_name']);

					// set image 
					$width  = $Himg_2->getWidth();
					$height = $Himg_2->getHeight();
					$centreX = round($width / 2);
					$centreY = round($height / 2);


					//----------------------------------------------//
					//---------------480x600-SECTION----------------//
					//----------------------------------------------//

					// scale image to 480x600 from max height
					$img2_Width = round(($height * 0.8) / 2); //scaling img width from height
					$img2_Height = round($height / 2);

					// positioning img 480x600 to crop
					$img2_x1 = $centreX - $img2_Width; 
					$img2_y1 = $centreY - $img2_Height; 
					$img2_x2 = $centreX + $img2_Width; 
					$img2_y2 = $centreY + $img2_Height;

					// cropping then resize image to 480x600 from center image
					$Himg_2->crop($img2_x1, $img2_y1, $img2_x2, $img2_y2);
					$Himg_2->resample(480, 600);


					//----------------------------------------------//
					//---------------768x576-SECTION----------------//
					//----------------------------------------------//

					// scale image to 768x576 from max height
					$img3_Width = round(((768 / 576) * $height) / 2); //scaling img width from height
					$img3_Height = round($height / 2);

					// positioning img 768x576 to crop
					$img3_x1 = $centreX - $img3_Width; 
					$img3_y1 = $centreY - $img3_Height; 
					$img3_x2 = $centreX + $img3_Width; 
					$img3_y2 = $centreY + $img3_Height;

					// cropping then resize image to 768x576 from center image
					$Himg_3->crop($img3_x1, $img3_y1, $img3_x2, $img3_y2);
					$Himg_3->resample(768, 576);


					//----------------------------------------------//
					//----------------UPLOAD-SECTION----------------//
					//----------------------------------------------//

					// saving file to uploads folder
					$Himg_1 = move_uploaded_file($_FILES["HimgInp"]["tmp_name"], $uploadTmp . $permalink . '-h' . $fileExtension);
					if ($Himg_1) {
						if (!$Himg_2->save($uploadTmp . $permalink . '-h-480x600' . $fileExtension)) {
							if (!$Himg_3->save($uploadTmp . $permalink . '-h-768x576' . $fileExtension)) {
								$Himg_1 = $permalink . '-h' . $fileExtension;
								$Himg_2 = $permalink . '-h-480x600' . $fileExtension;
								$Himg_3 = $permalink . '-h-768x576' . $fileExtension;
							} else {
								$pdo->rollback();
								http_response_code(404);
								include("../404.php");
								die();
							}
						} else {
							$pdo->rollback();
							http_response_code(404);
							include("../404.php");
							die();
						}
					} else {
						$pdo->rollback();
						http_response_code(404);
						include("../404.php");
						die();
					}
						
				}
			}

			// thumbnail image upload
			if ($_FILES['FimgInp']['error']  > 0 ) {
				if ($FimgInp_txt == $imgF) {
					$fileExtension = strrchr($imgF, ".");
					rename($uploadTmp . $imgF, $uploadTmp . $permalink . '-thumb' . $fileExtension);
					$Fimg = $permalink . '-thumb' . $fileExtension;
				} else if ($FimgInp_txt == "") {
					$fileExtension = ".jpg";
					$Fimg = copy($uploadTmp . "no-image-thumb.jpg", $uploadTmp . $permalink . '-thumb' . $fileExtension);
					$Fimg = $permalink . '-thumb' . $fileExtension;
				} 
			} else {
				// array of valid extensions
				$validExtensions = array('.jpg', '.jpeg', '.gif', '.png');
				// get extension of the uploaded file
				$fileExtension = strrchr($_FILES['FimgInp']['name'], ".");

				if (in_array($fileExtension, $validExtensions)) {
					// initialization image
					$Fimg = new ImageManipulator($_FILES['FimgInp']['tmp_name']);

					// set image 
					$width  = $Fimg->getWidth();
					$height = $Fimg->getHeight();
					$centreX = round($width / 2);
					$centreY = round($height / 2);

					// scale image to 720x405 from max height
					$img_Width = round(((720 / 405) * $height) / 2); //scaling img width from height
					$img_Height = round($height / 2);

					// positioning img 768x576 to crop
					$imgX1 = $centreX - $img_Width; 
					$imgY1 = $centreY - $img_Height; 
					$imgX2 = $centreX + $img_Width; 
					$imgY2 = $centreY + $img_Height;

					// cropping then resize image to 768x576 from center image
					$Fimg->crop($imgX1, $imgY1, $imgX2, $imgY2);
					$Fimg->resample(720, 405);

					if (!$Fimg->save($uploadTmp . $permalink . '-thumb' . $fileExtension)) {
						$Fimg = $permalink . '-thumb' . $fileExtension;
					} else {
						$pdo->rollback();
						http_response_code(404);
						include("../404.php");
					}
				}
			}

			// post image upload
			if ($_FILES['PimgInp']['error']  > 0 ) {
				if ($PimgInp_txt == $imgP) {
					$fileExtension = strrchr($imgP, ".");
					rename($uploadTmp . $imgP, $uploadTmp . $permalink . '-p' . $fileExtension);
					$Pimg = $permalink . '-p' . $fileExtension;
				} else if ($PimgInp_txt == "") {
					$fileExtension = ".jpg";
					$Pimg = copy($uploadTmp . "no-image-p.jpg", $uploadTmp . $permalink . '-p' . $fileExtension);
					$Pimg = $permalink . '-p' . $fileExtension;
				}
			} else {
				$fileExtension = strrchr($_FILES['PimgInp']['name'], ".");
				$Pimg = move_uploaded_file($_FILES["PimgInp"]["tmp_name"], $uploadTmp . $permalink . '-p' . $fileExtension);
				if (!$Pimg) {
					$pdo->rollback();
					http_response_code(404);
					include("../404.php");
					die();
				} else {
					$Pimg = $permalink . '-p' . $fileExtension;
				}
			}

			// update image table 
			$pimgUpdateQry = "UPDATE port_pimg SET `img_head1`=:imgh1, `img_head2`=:imgh2, `img_head3`=:imgh3, `img_thumb`=:imgthumb, `img_post`=:imgpost, `modified_by`=:user WHERE `post_id`=:id";
			$pimgUpdate = $pdo->prepare($pimgUpdateQry);
			$pimgUpdate->bindValue(":imgh1", $Himg_1, PDO::PARAM_STR);
			$pimgUpdate->bindValue(":imgh2", $Himg_2, PDO::PARAM_STR);
			$pimgUpdate->bindValue(":imgh3", $Himg_3, PDO::PARAM_STR);
			$pimgUpdate->bindValue(":imgthumb", $Fimg, PDO::PARAM_STR);
			$pimgUpdate->bindValue(":imgpost", $Pimg, PDO::PARAM_STR);
			$pimgUpdate->bindValue(":user", $user, PDO::PARAM_STR);
			$pimgUpdate->bindValue(":id", $postId, PDO::PARAM_STR);
			$pimgUpdate->execute();

			// move tmp image to post dir
			try {
				$moveH1 = unlink($uploadDir . $imgH1);
				if (!$moveH1) {
					throw new Exception('Can not moving header 1 image, probleme when delete');
				} else {
					$moveH1 = rename($uploadTmp . $Himg_1, $uploadDir . $Himg_1);
					if (!$moveH1) {
						throw new Exception('Failed moving header 1 image');
					}
				}

				$moveH2 = unlink($uploadDir . $imgH2);
				if (!$moveH2) {
					throw new Exception('Can not moving header 2 image, probleme when delete');
				} else {
					$moveH2 = rename($uploadTmp . $Himg_2, $uploadDir . $Himg_2);
					if (!$moveH2) {
						throw new Exception('Failed moving header 2 image');
					}
				}

				$moveH3 = unlink($uploadDir . $imgH3);
				if (!$moveH3) {
					throw new Exception('Can not moving header 3 image, probleme when delete');
				} else {
					$moveH3 = rename($uploadTmp . $Himg_3, $uploadDir . $Himg_3);
					if (!$moveH3) {
						throw new Exception('Failed moving header 3 image');
					}
				}

				$moveF = unlink($uploadDir . $imgF);
				if (!$moveF) {
					throw new Exception('Can not moving thumbnail image, probleme when delete');
				} else {
					$moveF = rename($uploadTmp . $Fimg, $uploadDir . $Fimg);
					if (!$moveF) {
						throw new Exception('Failed moving header 3 image');
					}
				}

				$moveP = unlink($uploadDir . $imgP);
				if (!$moveP) {
					throw new Exception('Can not moving post image, probleme when delete');
				} else {
					$moveP = rename($uploadTmp . $Pimg, $uploadDir . $Pimg);
					if (!$moveP) {
						throw new Exception('Failed moving header 3 image');
					}
				}

				$pdo->commit();

				$getLink = "SELECT cat_permalink FROM port_category WHERE cat_id = :id";
				$getLink = $pdo->prepare($getLink);
				$getLink->bindValue(":id", $category, PDO::PARAM_STR);
				$getLink->execute();
				$getLink = $getLink->fetchAll(PDO::FETCH_ASSOC);
				$getLink = $getLink[0]['cat_permalink'];
				header("location: $root/port-admin/$getLink");
			} catch (Exception $e) {
				$pdo->rollback();
				echo "<br>".$e;
				http_response_code(404);
				include("../404.php");
				die();
			}
		} catch (PDOException $e) {
			$pdo->rollback();
			header("location: $root/404.php");
		}
	}

	if(isset($_POST['delete-article_x'])) {
		$user = $_SESSION["access-login"];
		$post = $_POST['postlink'];
		$link = $_POST['catlink'];
		echo "cliked : " . $post;

		try {
			$deleteQRY = "UPDATE port_post SET status='delete', `modified_by`=:user WHERE `post_permalink`=:link";
			$deletePost = $pdo->prepare($deleteQRY);
			$deletePost->bindValue(":user", $user, PDO::PARAM_STR);
			$deletePost->bindValue(":link", $post, PDO::PARAM_STR);
			$deletePost->execute();

			header("location: $root/port-admin/$link");
		} catch (Exception $e) {
			echo $e;
			http_response_code(404);
			include("../404.php");
			die();
		}
	}

?>
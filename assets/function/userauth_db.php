<?php

	if(isset($_POST['user-login'])) {
		$user 		= $_POST['user'];
		$password 	= md5($_POST['pass']);

		//check user existed or not
		$qry = "SELECT user_username, user_password, status FROM port_user WHERE user_username=:user";
		$userCheck = $pdo->prepare($qry);
		$userCheck->bindValue(":user", $user, PDO::PARAM_STR);
		$userCheck->execute();

		if ($userCheck->rowCount() > 0) {
			$dbData = $userCheck->fetchAll(PDO::FETCH_ASSOC);
			$dbUser = $dbData[0]['user_username'];
			$dbPass = $dbData[0]['user_password'];
			$dbStat = $dbData[0]['status'];

			if ($password == $dbPass) {
				if ($dbStat == /*initial your admin status*/ OR $dbStat == /*initial your other admin status*/) {
					$_SESSION["userauth-for-admin_token-key"] = "" //add custom token;
					$_SESSION["access-login"] = $dbUser;
					header('location: port-admin/');
				} else {
					header('location: ?alert=login&result=notactive');
				}
			} else {
				header('location: ?alert=login&result=invalid');
			}
		} else {
			//check user existed or not
			$qry = "SELECT user_username, user_password, status FROM port_user WHERE user_email=:user";
			$userCheck = $pdo->prepare($qry);
			$userCheck->bindValue(":user", $user, PDO::PARAM_STR);
			$userCheck->execute();

			if ($userCheck->rowCount() > 0) {
				$dbData = $userCheck->fetchAll(PDO::FETCH_ASSOC);
				$dbUser = $dbData[0]['user_username'];
				$dbPass = $dbData[0]['user_password'];
				$dbStat = $dbData[0]['status'];

				if ($password == $dbPass) {
					if ($dbStat == 69 OR $dbStat == 8) {
						$_SESSION["userauth-for-admin_token-key"] = "" //add custom token;
						$_SESSION["access-login"] = $dbUser;
						header('location: port-admin/');
					} else {
						header('location: ?alert=login&result=notactive');
					}
				} else {
					header('location: ?alert=login&result=invalid');
				}
			} else {
				header('location: ?alert=login&result=notfound');
			}
		}
	}

	if(isset($_POST['post-user'])) {
		$root = BASE_URL;

		$fullname 	= $_POST['fullname'];
		$username 	= $_POST['username'];
		$email 		= $_POST['email'];
		$password 	= md5($_POST['password']);

		$chckUserQry= "SELECT user_username FROM port_user WHERE user_username = :username";
		$chckUser 	= $pdo->prepare($chckUserQry);
		$chckUser->bindValue(":username", $username, PDO::PARAM_STR);
		$chckUser->execute();
		if ($chckUser->rowCount() > 0) {
			header("location: $root/user/register?alert=user");
		} else {
			$chckUserQry= "SELECT user_email FROM port_user WHERE user_email = :email";
			$chckUser 	= $pdo->prepare($chckUserQry);
			$chckUser->bindValue(":email", $email, PDO::PARAM_STR);
			$chckUser->execute();
			if ($chckUser->rowCount() > 0) {
				header("location: $root/user/register?alert=email");
			} else {
				// begin transaction
				$pdo->beginTransaction();
				try {
					$addQry = "INSERT INTO port_user (user_username, user_fullname, user_email, user_password) VALUES (:username, :fullname, :email, :password) ";
					$addUser = $pdo->prepare($addQry);
					$addUser->bindValue(":username", $username, PDO::PARAM_STR);
					$addUser->bindValue(":fullname", $fullname, PDO::PARAM_STR);
					$addUser->bindValue(":email", $email, PDO::PARAM_STR);
					$addUser->bindValue(":password", $password, PDO::PARAM_STR);
					$addUser->execute();
					$pdo->commit();
					header("location: $root/user/register?alert=done");
				} catch (PDOException $e) {
					$pdo->rollback();
					header("location: $root/404.php");
				}
			}
		}
	}

?>
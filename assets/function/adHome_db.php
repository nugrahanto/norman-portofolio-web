<?php
	$result = 'null';
	$alert = 'null';

	if(isset($_POST['home-header-submit'])) {
		$id = 2018;
		$header = $_POST['header'];
		$desc = $_POST['desc'];

		try {
			$qry = "UPDATE webinfo SET `home-header`=:header, `home-desc`=:desc WHERE id=:id";
			$stmt = $pdo->prepare($qry);
			$stmt->bindValue(":header", $header, PDO::PARAM_STR);
			$stmt->bindValue(":desc", $desc, PDO::PARAM_STR);
			$stmt->bindValue(":id", $id, PDO::PARAM_STR);
			$stmt->execute();
			$alert = 'true';
			$result = 'successfully entered data';
		} catch(PDOException $e) {
			$alert = 'false';
			$result = 'entered data failed';
		}
	}

	elseif (isset($_POST['post-category'])) {
		$name 		= $_POST['name'];
		$desc 		= $_POST['desc'];
		$subDesc 	= $_POST['sub-desc'];

		//change post_name remove space to "-" for permalink use
		$permalink	= fungsi_name($name);

		$fileName 	= $_FILES['imgInp']['name'];
		$fileSize 	= $_FILES['imgInp']['size']; 
		$fileError 	= $_FILES['imgInp']['error'];
		$fileExt 	= pathinfo($_FILES['imgInp']['name'], PATHINFO_EXTENSION);

		//prepare image filename with category name
		$targetDir 	= "../img/cat/";
		$newName 	= $permalink.".".$fileExt;
		$targetFile = $targetDir . $newName;

		//check category existed or not
		$check = "SELECT cat_permalink FROM port_category WHERE cat_permalink=:permalink";
		$checkStmt = $pdo->prepare($check);
		$checkStmt->bindValue(":permalink", $permalink, PDO::PARAM_STR);
		$checkStmt->execute();

		if ($checkStmt->rowCount() > 0) {
			$alert = 'false';
			$result = 'category already existed';
		} else {

			//check any attachement image or not
			if($fileSize > 0 || $fileError == 0){

				//upload image
				$move = move_uploaded_file($_FILES["imgInp"]["tmp_name"], $targetFile);
				if ($move) {
					try {
						//inserting data to category table
						$qry = "INSERT INTO port_category (cat_name,cat_permalink,cat_desc,cat_subdesc,cat_img) VALUES (:name,:permalink,:desc,:subdesc,:img)";
						$stmt = $pdo->prepare($qry);
						$stmt->bindValue(":name", $name, PDO::PARAM_STR);
						$stmt->bindValue(":permalink", $permalink, PDO::PARAM_STR);
						$stmt->bindValue(":desc", $desc, PDO::PARAM_STR);
						$stmt->bindValue(":subdesc", $subDesc, PDO::PARAM_STR);
						$stmt->bindValue(":img", $newName, PDO::PARAM_STR);
						$stmt->execute();

						$alert = 'true';
						$result = 'category was successfully created';
					} catch (PDOException $e) {
						//delete image from server
						if (file_exists($targetFile)) {
							unlink($targetFile);
						}

						$alert = 'false';
						$result = 'failed to create category';
					}
				} else {
					$alert = 'false';
					$result = 'failed to upload image';
				}
			} else {
				try {
					//inserting data to category table
					$qry = "INSERT INTO port_category (cat_name,cat_permalink,cat_desc,cat_subdesc,cat_img) VALUES (:name,:permalink,:desc,:subdesc,:img)";
					$stmt = $pdo->prepare($qry);
					$stmt->bindValue(":name", $name, PDO::PARAM_STR);
					$stmt->bindValue(":permalink", $permalink, PDO::PARAM_STR);
					$stmt->bindValue(":desc", $desc, PDO::PARAM_STR);
					$stmt->bindValue(":subdesc", $subDesc, PDO::PARAM_STR);
					$stmt->bindValue(":img", "no-image.jpg", PDO::PARAM_STR);
					$stmt->execute();

					$alert = 'true';
					$result = 'category was successfully created';
				} catch (PDOException $e) {
					$alert = 'false';
					$result = 'failed to create category';
				}
			}

		}
	}

	elseif (isset($_POST['put-category'])) {
		$id			= $_POST['catId'];
		$name 		= $_POST['name'];
		$desc 		= $_POST['desc'];
		$subDesc 	= $_POST['sub-desc'];

		//change post_name remove space to "-" for permalink use
		$permalink	= fungsi_name($name);

		$fileName 	= $_FILES['imgInp']['name'];
		$fileSize 	= $_FILES['imgInp']['size']; 
		$fileError 	= $_FILES['imgInp']['error'];
		$fileExt 	= pathinfo($_FILES['imgInp']['name'], PATHINFO_EXTENSION);

		//check any attachement image or not
		if($fileSize > 0 || $fileError == 0){ 
			//prepare image filename with category name
			$targetDir 	= "../img/cat/";
			$newName 	= $permalink.".".$fileExt;
			$targetFile = $targetDir . $newName;

			//upload image
			$move = move_uploaded_file($_FILES["imgInp"]["tmp_name"], $targetFile);
			if ($move) {
				try {
					//updating data without img to category table
					$qry = "UPDATE port_category SET `cat_name`=:name, `cat_permalink`=:permalink, `cat_desc`=:desc, `cat_subdesc`=:subdesc, `cat_img`=:img WHERE `cat_id`=:id";
					$stmt = $pdo->prepare($qry);
					$stmt->bindValue(":id", $id, PDO::PARAM_STR);
					$stmt->bindValue(":name", $name, PDO::PARAM_STR);
					$stmt->bindValue(":permalink", $permalink, PDO::PARAM_STR);
					$stmt->bindValue(":desc", $desc, PDO::PARAM_STR);
					$stmt->bindValue(":subdesc", $subDesc, PDO::PARAM_STR);
					$stmt->bindValue(":img", $newName, PDO::PARAM_STR);
					$stmt->execute();

					$alert = 'true';
					$result = 'successfully updated category';
				} catch (PDOException $e) {
					$alert = 'false';
					$result = 'failed to update category';
				}
			} else {
				$alert = 'false';
				$result = 'failed to upload image';
			}
		} else {
			try {
				//updating data without img to category table
				$qry = "UPDATE port_category SET `cat_name`=:name, `cat_permalink`=:permalink, `cat_desc`=:desc, `cat_subdesc`=:subdesc WHERE `cat_id`=:id";
				$stmt = $pdo->prepare($qry);
				$stmt->bindValue(":id", $id, PDO::PARAM_STR);
				$stmt->bindValue(":name", $name, PDO::PARAM_STR);
				$stmt->bindValue(":permalink", $permalink, PDO::PARAM_STR);
				$stmt->bindValue(":desc", $desc, PDO::PARAM_STR);
				$stmt->bindValue(":subdesc", $subDesc, PDO::PARAM_STR);
				$stmt->execute();

				$alert = 'true';
				$result = 'successfully updated category';
			} catch (PDOException $e) {
				$alert = 'false';
				$result = 'failed to update category';
			}
		}
	}

?>



function inputReadURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#imgInp_txt').val(input.files[0].name);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

function inputEReadURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#imgInpEdit_txt').val(input.files[0].name);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

function HreadURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#h-img-upload').attr('src', e.target.result);
			$('#HimgInp_txt').val(input.files[0].name);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

function PreadURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#p-img-upload').attr('src', e.target.result);
			$('#PimgInp_txt').val(input.files[0].name);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

function FreadURL(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#f-img-upload').attr('src', e.target.result);
			$('#FimgInp_txt').val(input.files[0].name);
		}

		reader.readAsDataURL(input.files[0]);
	}
}

$("#HimgInp").change(function() {
  HreadURL(this);
});

$("#PimgInp").change(function() {
  PreadURL(this);
});

$("#FimgInp").change(function() {
  FreadURL(this);
});

$("#imgInp").change(function() {
  inputReadURL(this);
});

$("#imgInpEdit").change(function() {
  inputEReadURL(this);
});
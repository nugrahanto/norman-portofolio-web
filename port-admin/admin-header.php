<?php
  error_reporting(0);
  session_start();

  include("../assets/function/koneksi_db.php");
  include("../assets/function/fungsi_name.php");

  //check auth
  if (isset($_SESSION["userauth-for-admin_token-key"]) AND $_SESSION["userauth-for-admin_token-key"] == 'userauth-ok') {
    
  } else {
    session_destroy(); 
    header("location: " . BASE_URL);
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/icon/favicon.png">

    <title>Norman Works</title>

    <!-- Base website -->
    <base href="<?php echo BASE_URL; ?>">

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="assets/css/carousel.css">
    <link rel="stylesheet" type="text/css" href="assets/css/admin.css">
    <link rel="stylesheet" type="text/css" href="assets/css/portfolio.css">
  </head>
<!-- NAVBAR
================================================== -->
  <body style="background-color: #555">
    <div id="bg">
      <img src="assets/img/darkgon.png">
    </div>

    <?php 
      $catStmt = $pdo->prepare('SELECT * FROM port_category WHERE status="publish"');
      $catStmt->execute();

      $catFetch = $catStmt->fetchAll(PDO::FETCH_ASSOC);
    ?>

    <div class="navbar-wrapper">
      <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a role="button" class="navbar-brand">Admin Page</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="<?php BASE_URL ?>port-admin/">Home</a></li>
                <?php 
                  foreach ($catFetch as $headData) {
                    $name       = $headData['cat_name'];
                    $permalink  = $headData['cat_permalink'];
                ?>
                  <li><a href="port-admin/<?php echo $permalink ?>"><?php echo $name; ?></a></li>
                <?php } ?>
              </ul>
              <ul class="navbar-right"></ul>
              <ul class="nav navbar-nav navbar-right login-form_wrapper">
                <li class="active"><a href="<?php BASE_URL ?>" id="logout" >view web</a></li>
                <li class="active"><a href="<?php BASE_URL ?>logout.php">logout</a></li>
              </ul>
            </div>
          </div>
        </nav>

      </div>
    </div>
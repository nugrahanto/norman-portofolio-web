<?php 
  include ('admin-header.php');
  include ('../assets/function/adPost_db.php');
?>

    <main class="main main-flush" style="background-color: transparent !important">

      <!-- Post Header
      ================================================== -->
      <!-- /.post header -->

      <div class="post-wrapper">
        <div class="ad-post__constraint">
          <section class="post-content-wrapper">
            <?php 
              $var = $_GET['var'];
              $cat = $_GET['cat'];

              if ($var == 'post-add') : ?>

              <h1 class="h-header-wrapper page__heading" style="padding-bottom: 20px;">ADD NEW POST</h1>

              <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="category" value="<?php echo $cat ?>">
                <div class="post-content">

                  <div class="form-group">
                    <label style="color: #00ffff">Upload Header Image ( Recommended : 1920x780 And Focus On The Center )</label>
                    <div class="input-group">
                      <span class="input-group-btn">
                        <span class="btn btn-default btn-file">
                          Browse… <input type="file" id="HimgInp" name="HimgInp" value="" accept="image/*">
                        </span>
                      </span>
                      <input type="text" id="HimgInp_txt" class="form-control" value="" readonly>
                    </div>

                    <div class="post-content-width">
                      <div class="ad-post__header-img">
                        <a onclick="delTempHImg()" class="db-notif__button"><img src="assets/icon/close-grey.png"></a>
                        <img id='h-img-upload' src="img/tmp/no-image-h.jpg" />
                      </div>
                    </div>
                  </div>

                  <hr>

                  <div class="post-content-width post-content-picture">
                    <div class="form-group">
                      <label style="color: #00ffff">Upload Post Image</label>
                      <div class="input-group">
                        <span class="input-group-btn">
                          <span class="btn btn-default btn-file">
                            Browse… <input type="file" id="PimgInp" name="PimgInp" value="" accept="image/*">
                          </span>
                        </span>
                        <input type="text" id="PimgInp_txt" class="form-control" value="" readonly>
                      </div>

                      <div class="post-image">
                        <a onclick="delTempPImg()" class="db-notif__button"><img src="assets/icon/close-grey.png"></a>
                        <img id='p-img-upload' src="img/tmp/no-image-p.jpg" />
                      </div>
                    </div><!-- /.form-group -->
                  </div><!-- /.post-content-picture -->

                  <div class="post-content-width post-content-desc">
                    <article class="post-content-article">
                      <div class="post-content-article-info ad-post__form">
                        <dt>Header </dt>
                        <input class="form-control capZ" type="text" name="header" placeholder="Post Header" required>
                      </div><!-- /.post-content-article-header -->
                      <div class="post-content-article-info ad-post__form">
                        <dt>Client</dt>
                        <input class="form-control capZ" type="text" name="client" placeholder="Client">
                        <dt>Project</dt>
                        <input class="form-control capZ" type="text" name="project" placeholder="Project Type" value="<?php echo $cat ?>">
                        <dt>Location</dt>
                        <input class="form-control capZ" type="text" name="location" placeholder="Client Location">
                      </div><!-- /.post-content-article-info -->
                      <div class="post-content-article-detail">
                        <h3>Project Detail</h3>
                        <textarea class="form-control" name="detail" placeholder="Project Detail" rows="10" style="color: #000; font-weight: bold;"></textarea>
                      </div><!-- /.post-content-article-detail -->
                      <div class="post-content-article-share">
                        <div class="form-group">
                          <label style="color: #00ffff">Upload Thumbnail Image : <br> ( Recommended : 720x405 )</label>
                          <div class="input-group">
                            <span class="input-group-btn">
                              <span class="btn btn-default btn-file">
                                Browse… <input type="file" id="FimgInp" name="FimgInp" value="" accept="image/*">
                              </span>
                            </span>
                            <input type="text" id="FimgInp_txt" class="form-control" value="" readonly>
                          </div>

                          <div class="post-image">
                            <a onclick="delTempFImg()" class="db-notif__button"><img src="assets/icon/close-grey.png"></a>
                            <img id='f-img-upload' src="img/tmp/no-image-thumb.jpg" />
                          </div>
                        </div><!-- /.form-group -->
                        <input type="submit" name="post-article" class="ad-form__button" value="save" style="padding: 5px 15px; float: right; margin-right: 0px !important">
                      </div>
                    </article>
                  </div><!-- /.post-content-desc -->
                </div><!-- /.post-content -->

              </form>

            <?php elseif ($var == 'post-edit') : 
              $url = $_GET['url'];

              // get post
              $postQRY = "SELECT port_post.*, port_pimg.*  FROM port_post JOIN port_category ON port_post.cat_id = port_category.cat_id JOIN port_pimg ON port_post.post_id = port_pimg.post_id WHERE port_post.post_permalink = :permalink AND port_category.cat_permalink = :catlink AND port_post.status = 'publish' LIMIT 1";
              $getPost = $pdo->prepare($postQRY);
              $getPost->bindValue(":permalink", $url, PDO::PARAM_STR);
              $getPost->bindValue(":catlink", $cat, PDO::PARAM_STR);
              $getPost->execute();

              // check post existed or not
              if ($getPost->rowCount() == 0) {
                http_response_code(404);
                include("../404.php");
                die();
              } else {
                $getPost = $getPost->fetchAll(PDO::FETCH_ASSOC);
                foreach ($getPost as $result) {
                  $postId       = $result['post_id'];
                  $catId        = $result['cat_id'];
                  $header       = $result['post_head'];
                  $client       = $result['post_client'];
                  $project      = $result['post_project'];
                  $location     = $result['post_location'];
                  $desc         = $result['post_desc'];
                  $status       = $result['status'];
                  $img_head1    = $result['img_head1'];
                  $img_thumb    = $result['img_thumb'];
                  $img_post     = $result['img_post'];
                }
            ?>
                <h1 class="h-header-wrapper page__heading" style="padding-bottom: 20px;">EDIT POST</h1>
                <form action="" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="postId" value="<?php echo $postId ?>">
                  <input type="hidden" name="category" value="<?php echo $catId ?>">
                  <input type="hidden" name="dbhead" value="<?php echo $header ?>">
                  <input type="hidden" name="permalink" value="<?php echo $url ?>">
                  <input type="hidden" name="Himg_value" id="Himg_value" value="<?php echo $img_head1 ?>">
                  <input type="hidden" name="Pimg_value" id="Pimg_value" value="<?php echo $img_post ?>">
                  <input type="hidden" name="Fimg_value" id="Fimg_value" value="<?php echo $img_thumb ?>">
                  <div class="post-content">

                    <div class="form-group">
                      <label style="color: #00ffff">Upload Header Image ( Recommended : 1920x780 And Focus On The Center )</label>
                      <div class="input-group">
                        <span class="input-group-btn">
                          <span class="btn btn-default btn-file">
                            Browse… <input type="file" id="HimgInp" name="HimgInp" value="" accept="image/*">
                          </span>
                        </span>
                        <input type="text" id="HimgInp_txt" class="form-control" value="<?php echo $img_head1 ?>" readonly>
                      </div>

                      <div class="post-content-width">
                        <div class="ad-post__header-img">
                          <a onclick="delTempHImg()" class="db-notif__button-left"><img src="assets/icon/delete.png"></a>
                          <a onclick="cancelHImg('<?php echo $img_head1 ?>')" class="db-notif__button"><img src="assets/icon/close-grey.png"></a>
                          <img id='h-img-upload' src="img/pst/<?php echo $img_head1 ?>?filemtime(img/pst/<?php echo $img_head1 ?>)" />
                        </div>
                      </div>
                    </div>

                    <hr>

                    <div class="post-content-width post-content-picture">
                      <div class="form-group">
                        <label style="color: #00ffff">Upload Post Image</label>
                        <div class="input-group">
                          <span class="input-group-btn">
                            <span class="btn btn-default btn-file">
                              Browse… <input type="file" id="PimgInp" name="PimgInp" value="" accept="image/*">
                            </span>
                          </span>
                          <input type="text" id="PimgInp_txt" class="form-control" value="<?php echo $img_post ?>" readonly>
                        </div>

                        <div class="post-image">
                          <a onclick="delTempPImg()" class="db-notif__button-left"><img src="assets/icon/delete.png"></a>
                          <a onclick="cancelPImg('<?php echo $img_post ?>')" class="db-notif__button"><img src="assets/icon/close-grey.png"></a>
                          <img id='p-img-upload' src="img/pst/<?php echo $img_post ?>?filemtime(img/pst/<?php echo $img_post ?>)" />
                        </div>
                      </div><!-- /.form-group -->
                    </div><!-- /.post-content-picture -->

                    <div class="post-content-width post-content-desc">
                      <article class="post-content-article">
                        <div class="post-content-article-info ad-post__form">
                          <dt>Header </dt>
                          <input class="form-control capZ" type="text" name="header" value="<?php echo $header ?>" placeholder="Post Header" required>
                        </div><!-- /.post-content-article-header -->
                        <div class="post-content-article-info ad-post__form">
                          <dt>Client</dt>
                          <input class="form-control capZ" type="text" name="client" value="<?php echo $client ?>" placeholder="Client">
                          <dt>Project</dt>
                          <input class="form-control capZ" type="text" name="project" value="<?php echo $project ?>" placeholder="Project Type" value="<?php echo $cat ?>">
                          <dt>Location</dt>
                          <input class="form-control capZ" type="text" name="location" value="<?php echo $location ?>" placeholder="Client Location">
                        </div><!-- /.post-content-article-info -->
                        <div class="post-content-article-detail">
                          <h3>Project Detail</h3>
                          <textarea class="form-control" name="detail" placeholder="Project Detail" rows="10" style="color: #000; font-weight: bold;"><?php echo $desc; ?></textarea>
                        </div><!-- /.post-content-article-detail -->
                        <div class="post-content-article-share">
                          <div class="form-group">
                            <label style="color: #00ffff">Upload Thumbnail Image : <br> ( Recommended : 720x405 )</label>
                            <div class="input-group">
                              <span class="input-group-btn">
                                <span class="btn btn-default btn-file">
                                  Browse… <input type="file" id="FimgInp" name="FimgInp" value="" accept="image/*">
                                </span>
                              </span>
                              <input type="text" id="FimgInp_txt" class="form-control" value="<?php echo $img_thumb ?>" readonly>
                            </div>

                            <div class="post-image">
                              <a onclick="delTempFImg()" class="db-notif__button-left"><img src="assets/icon/delete.png"></a>
                              <a onclick="cancelFImg('<?php echo $img_thumb ?>')" class="db-notif__button"><img src="assets/icon/close-grey.png"></a>
                              <img id='f-img-upload' src="img/pst/<?php echo $img_thumb ?>?filemtime(img/pst/<?php echo $img_thumb ?>)" />
                            </div>
                          </div><!-- /.form-group -->
                          <input type="submit" name="put-article" class="ad-form__button" value="save" style="padding: 5px 15px; float: right; margin-right: 0px !important">
                        </div>
                      </article>
                    </div><!-- /.post-content-desc -->
                  </div><!-- /.post-content -->

                </form>

            <?php }
            else :
              http_response_code(404);
              include("../404.php");
              die();

            endif ?>

          </section>
        </div>
      </div>

      <script>
        function delTempHImg() {
          var element = document.getElementById("Himg_value");
          if (element) {
            element.value = "";
          }

          $('#HimgInp').val("");
          $('#HimgInp_txt').val("");
          $('#h-img-upload').attr('src', 'img/tmp/no-image-h.jpg');
        }

        function delTempPImg() {  
          var element = document.getElementById("Pimg_value");
          if (element) {
            element.value = "";
          }     

          $('#PimgInp').val("");
          $('#PimgInp_txt').val("");
          $('#p-img-upload').attr('src', 'img/tmp/no-image-h.jpg');
        }

        function delTempFImg() { 
          var element = document.getElementById("Fimg_value");
          if (element) {
            element.value = "";
          }

          $('#FimgInp').val("");
          $('#FimgInp_txt').val("");
          $('#f-img-upload').attr('src', 'img/tmp/no-image-thumb.jpg');
        }

        function cancelHImg(v) {
          var src = "img/pst/" + v;
          $('#HimgInp').val("");
          $('#HimgInp_txt').val(v);
          $('#h-img-upload').attr('src', src);
        }

        function cancelPImg(v) {
          var src = "img/pst/" + v;
          $('#PimgInp').val("");
          $('#PimgInp_txt').val(v);
          $('#p-img-upload').attr('src', src);
        }

        function cancelFImg(v) {
          var src = "img/pst/" + v;
          $('#FimgInp').val("");
          $('#FimgInp_txt').val(v);
          $('#f-img-upload').attr('src', src);
        }
      </script>

    </main>

<?php include ('admin-footer.php'); ?>

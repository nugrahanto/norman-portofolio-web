<?php 
  include ('admin-header.php'); 
  include ('../assets/function/adHome_db.php');
 
  $stmt = $pdo->prepare('SELECT * FROM webinfo');
  $stmt->execute();
  $web = $stmt->fetchAll(PDO::FETCH_ASSOC);
  $webHead = $web[0]['home-header'];
  $webDesc = $web[0]['home-desc'];
?>

    <main class="main home-flush">

      <!-- Notification
      ================================================== -->
      <?php  if ($alert == 'true') : ?>
        <div class="db-notif__container">
          <div class="db-notif__wrapper">
            <a href="./port-admin" class="db-notif__button"><img src="assets/icon/close-grey.png"></a>
            <div class="db-notif__icon"><img src="assets/icon/success.png"></div>
            <div class="db-notif__text"><?php echo $result ?></div>
          </div>
        </div>
      <?php elseif ($alert == 'false') : ?>
        <div class="db-notif__container">
          <div class="db-notif__wrapper">
            <a href="./port-admin" class="db-notif__button"><img src="assets/icon/close-grey.png"></a>
            <div class="db-notif__icon"><img src="assets/icon/failed.png"></div>
            <div class="db-notif__text"><?php echo $result ?></div>
          </div>
        </div>
      <?php endif ?>

      <!-- Home Header
      ================================================== -->
      <div class="home-constraint">

        <header class="home-header">

          <form action="port-admin/" method="post" class="ad-form__position">
            <div class="ad-grid__wrapper ad-form__title">edit your header text</div>
            <div class="ad-grid__wrapper ad-grid__heading">Header: </div>
            <div class="ad-grid__wrapper ad-grid__subheading"><input type="text" name="header" class="form-control" style="font-size: inherit !important;" placeholder="Home Header" value="<?php echo $webHead ?>"></div>

            <div class="ad-grid__wrapper ad-grid__heading">Description: </div>
            <div class="ad-grid__wrapper ad-grid__subheading"><textarea name="desc" rows="5" class="form-control" style="font-size: inherit !important;" placeholder="Home Description"><?php echo $webDesc ?></textarea></div>
            <input type="submit" class="ad-form__button button-clear" name="home-header-submit" style="padding: 5px 15px; float: right;">
            <input type="reset" onclick="hideContent();" value="cancel" class="ad-form__button" style="cursor: pointer; float: right;">
          </form>

          <h1 class="h-header-wrapper h-header__heading"><?php echo $webHead ?></h1>
          <p class="h-header-wrapper h-header__subheading"><?php echo $webDesc ?></p>
        </header>
      </div>
      <!-- /.Home Header -->

      <hr style="border: 1px dashed #555;">


      <!-- Add Category Form
      ================================================== -->
      <div class="home-constraint">
        <div class="ad-add__form-cat-wrapper">

          <div id="show-content" style="display: block;">
          <div class="ad-add__cat-wrapper">
            <button onclick="showContent();" class="ad-add__categories">
              <div class="ad-add__c-title-wrapper">
                <div class="ad-add__c-title">add category</div>
              </div>
              <img src="assets/icon/add-button.png">
            </button>
          </div>
          </div>

          <div id="hide-content" style="display: none;">
            <form action="port-admin/" method="post" class="ad-form__position" enctype="multipart/form-data">
              <div class="ad-grid__wrapper ad-form__title">insert category</div>
              <div class="ad-grid__wrapper ad-grid__heading">Name: </div>
              <div class="ad-grid__wrapper ad-grid__subheading"><input type="text" name="name" class="form-control" style="font-size: inherit !important;" placeholder="category name" required></div>
              <div class="ad-grid__wrapper ad-grid__heading">Description: </div>
              <div class="ad-grid__wrapper ad-grid__subheading"><textarea name="desc" rows="5" class="form-control" style="font-size: inherit !important;" placeholder="category description will show in homepage below category name"></textarea></div>
              <div class="ad-grid__wrapper ad-grid__heading">Sub-Description: </div>
              <div class="ad-grid__wrapper ad-grid__subheading" style="margin-top: 4px;"><textarea name="sub-desc" rows="5" class="form-control" style="font-size: inherit !important;" placeholder="category sub-description will show in category page"></textarea></div>
              <div class="ad-grid__wrapper ad-grid__heading">Image: </div>
              <div class="ad-grid__wrapper ad-grid__subheading">
                <div class="input-group">
                    <span class="input-group-btn">
                      <span class="btn btn-default btn-file" style="font-weight: bold !important;">Browse… <input type="file" id="imgInp" name="imgInp" accept="image/*"></span>
                    </span>
                  <input type="text" id="imgInp_txt" class="form-control" readonly>
                </div>
              </div>
              <input type="submit" class="ad-form__button" value="save" name="post-category" style="padding: 5px 15px; float: right;">
              <input type="reset" onclick="hideContent();" value="cancel" class="ad-form__button" style="cursor: pointer; float: right;">
            </form>
          </div>

        </div>
      </div>
      <!-- /.Add Category Form -->

      <!-- Categories List
      ================================================== -->
      <div class="home-constraint">
        <section class="h-content-wrapper h-content">

          <?php

            $rowMax = $catStmt->rowCount();
            $row = 0;
            
            //fetch data
            foreach ($catFetch as $data) {
              $row        += 1;
              $id         = $data['cat_id'];
              $name       = $data['cat_name'];
              $permalink  = $data['cat_permalink'];
              $desc       = $data['cat_desc'];
              $subdesc    = $data['cat_subdesc'];
              $img        = $data['cat_img'];
              $status     = $data['status'];

          ?>

            <article class="h-categories__item">
              <div class="ad-categories__wrapper" >
                <a href="port-admin/<?php echo $permalink ?>" class="ad-categories__info-wrapper" title="<?php  echo $name ?>">
                  <div class="h-content-wrapper h-content ad-categories__info">
                    <h2 class="ad-categories__title"><?php echo $name ?></h2>
                    <div class="ad-categories__detail"><p><?php echo $desc ?></p></div>
                  </div>
                </a>
                <button onclick="showCatContent(<?php echo "$row, $rowMax"; ?>)" class="ad-categories__button">edit category</button>
                <picture class="h-categories__picture">
                  <source srcset="img/cat/<?php echo $img ?>?filemtime(img/cat/<?php echo $img ?>)" media="(min-width: 48em)">
                  <a href="port-admin/<?php echo $permalink ?>" title="<?php  echo $name ?>">
                    <img class="h-categories__item-image" srcset src="img/cat/<?php echo $img ?>?filemtime(img/cat/<?php echo $img ?>)">
                  </a>
                </picture>
              </div><!-- /.h-categories__item-link -->
              <div id="<?php echo('cat-content-'.$row) ?>" style="margin-top: 20px; display: none;">
                <form action="port-admin/" method="post" class="ad-form__position" enctype="multipart/form-data">
                  <input type="hidden" name="catId" value="<?php echo $id ?>">
                  <div class="ad-grid__wrapper ad-form__title">edit <?php  echo $name ?> category</div>
                  <div class="ad-grid__wrapper ad-grid__heading">Name: </div>
                  <div class="ad-grid__wrapper ad-grid__subheading">
                    <input type="text" name="name" class="form-control" style="font-size: inherit !important;" placeholder="category name" value="<?php echo $name ?>">
                  </div>
                  <div class="ad-grid__wrapper ad-grid__heading">Description: </div>
                  <div class="ad-grid__wrapper ad-grid__subheading">
                    <textarea name="desc" rows="5" class="form-control" style="font-size: inherit !important;" placeholder="category description will show in homepage below category name"><?php echo $desc ?></textarea>
                  </div>
                  <div class="ad-grid__wrapper ad-grid__heading">Sub-Description: </div>
                  <div class="ad-grid__wrapper ad-grid__subheading" style="margin-top: 4px;">
                    <textarea name="sub-desc" rows="5" class="form-control" style="font-size: inherit !important;" placeholder="category sub-description will show in category page"><?php echo $subdesc; ?></textarea>
                  </div>
                  <div class="ad-grid__wrapper ad-grid__heading">Image: </div>
                  <div class="ad-grid__wrapper ad-grid__subheading">
                    <div class="input-group">
                        <span class="input-group-btn">
                          <span class="btn btn-default btn-file" style="font-weight: bold !important;">Browse… <input type="file" id="imgInpEdit" name="imgInp" accept="image/*" ></span>
                        </span>
                      <input type="text" id="imgInpEdit_txt" class="form-control" readonly>
                    </div>
                  </div>
                  <input type="submit" class="ad-form__button" value="save" name="put-category" style="padding: 5px 15px; float: right;">
                  <input type="reset" onclick="hideCatContent(<?php echo "$row"; ?>);" class="ad-form__button" value="cancel" style="cursor: pointer; float: right;">
                </form>
              </div>
            </article>

          <?php
            }
          ?>
          

          <script>
            function showCatContent(param, max) {
              var element = document.getElementById("cat-content-"+param);

              if (element.style.display == "none") {
                for (var i = 1; i <= max; i++) {
                  document.getElementById("cat-content-"+i).style.display = "none";
                }

                element.style.display = "block";
              }
              else {
                element.style.display = "none";
              }

            }

            function hideCatContent(param) {
              document.getElementById("cat-content-"+param).style.display = "none";
            }
          </script>

        </section>

      </div>
      <!-- /.Categories List -->

    </main>

<?php include ('admin-footer.php'); ?>
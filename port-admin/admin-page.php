<?php 

  include ('admin-header.php');
  include ('../assets/function/adPost_db.php');

  $pageLink = $_GET['url'];
  $getCatStmt = $pdo->prepare('SELECT * FROM port_category WHERE cat_permalink = :permalink');
  $getCatStmt->bindValue(":permalink", $pageLink, PDO::PARAM_STR);
  $getCatStmt->execute();

  if ($getCatStmt->rowCount() > 0) :
  
  $getCat = $getCatStmt->fetchAll(PDO::FETCH_ASSOC);
  $pageId   = $getCat[0]['cat_id'];
  $pageHead = $getCat[0]['cat_name'];
  $pageDesc = $getCat[0]['cat_subdesc'];

?>


    <main class="ad-main home-flush">

      <!-- Post Header
      ================================================== -->
      <div class="home-constraint">
        <header class="home-header">
          <h1 class="h-header-wrapper page__heading"><?php echo $pageHead; ?></h1>
          <p class="h-header-wrapper page__subheading"><?php echo $pageDesc; ?></p>
        </header>
      </div>
      <!-- /.post header -->

      <div class="home-constraint">
        <section class="h-content-wrapper h-content page-bg-color">

        	<div class="ad-post__item-button">
        		<a href="port-admin/<?php echo $pageLink ?>/post-add" class="ad-post__button-link">
                    <div class="ad-post__button-info">
                        <header class="ad-post__button-title">add post</header>
                        <img src="assets/icon/add-button.png">
                    </div>
                </a>
        	</div>

          <!-- LOOP HERE -->
          <?php 
            $postQry = "SELECT port_post.post_head, port_post.post_permalink, port_pimg.img_thumb FROM port_post JOIN port_pimg ON port_post.post_id = port_pimg.post_id WHERE port_post.cat_id = :catid AND port_post.status='publish'";
            $getPost = $pdo->prepare($postQry);
            $getPost->bindValue(":catid", $pageId, PDO::PARAM_STR);
            $getPost->execute();
            $getPost = $getPost->fetchAll(PDO::FETCH_ASSOC);

            foreach ($getPost as $data) {
          ?>
            <div class="page-item__grid-3">
              <form action="" method="post">
                <input type="hidden" name="postlink" value="<?php echo $data['post_permalink'] ?>">
                <input type="hidden" name="catlink" value="<?php echo $pageLink ?>">
                <input type="image" name="delete-article" src="assets/icon/close-grey.png" class="db-notif__button" value="" >
              </form>
              <a href="port-admin/<?php echo $pageLink ?>/<?php echo $data['post_permalink'] ?>/post-edit" class="page-item__link">
                <header class="page-item__info">
                  <h2 class="page-item__title"><?php echo $data['post_head']; ?></h2>
                </header>
                <picture>
                  <img srcset src = "img/pst/<?php echo $data['img_thumb'] ?>?filemtime(img/pst/<?php echo $data['img_thumb'] ?>)">
                </picture>
              </a>
            </div>
          <?php
            }
          ?>

        </section>
      </div>

    </main>

  <?php else :
    http_response_code(404);
    include("../404.php");
    die();
  
  endif ?>

<?php include ('admin-footer.php'); ?>
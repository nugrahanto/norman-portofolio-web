<?php 
	include_once("assets/function/koneksi_db.php");
	include("assets/function/userauth_db.php");
 ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="assets/icon/favicon.png">

	<title>Norman Works</title>

	<!-- Base website -->
	<base href="<?php echo BASE_URL; ?>">

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

	<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	<script src="assets/js/ie-emulation-modes-warning.js"></script>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
	<script src="dist/js/bootstrap.min.js"></script>
</head>

<div class="container">
	<br><br>

	<div class="card bg-light">
		<article class="card-body mx-auto" style="max-width: 600px;">
			<?php 
				if (isset($_GET['alert'])) {
					$regAlert = $_GET['alert'];

					if ($regAlert == "user") :
			?>
				<p class="card-title mt-3 text-center">* Sorry, username already existed</p>
			<?php elseif ($regAlert == "email") : ?>
				<p class="card-title mt-3 text-center">* Sorry, email already existed</p>
			<?php elseif ($regAlert == "done") : ?>
				<p class="card-title mt-3 text-center">* Registration succes, <a href="<?php echo BASE_URL ?>">click here</a> to login</p>
			<?php endif ?>
			<?php
				}	 
			?>	
			
			<h4 class="card-title mt-3 text-center">Create Account</h4>
			<form action="" method="POST">
				<div class="form-group input-group">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input name="fullname" minlength=5 class="form-control" placeholder="Full name" type="text" required >
				</div> <!-- form-group// -->
				<div class="form-group input-group">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-user"></i> </span>
					</div>
					<input name="username" minlength=5 class="form-control" placeholder="Username" type="text" required >
				</div> <!-- form-group// -->
				<div class="form-group input-group">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
					</div>
					<input name="email" class="form-control" placeholder="Email address" type="email" required >
				</div> <!-- form-group// -->
				<div class="form-group input-group">
					<div class="input-group-prepend">
						<span class="input-group-text"> <i class="fa fa-lock"></i> </span>
					</div>
					<input name="password" minlength=5 class="form-control" placeholder="Create password" type="password" required >
				</div> <!-- form-group// -->
				<div class="form-group">
					<button type="submit" name="post-user" class="btn btn-primary btn-block"> Create Account  </button>
				</div> <!-- form-group// -->
				<p class="text-center">Have an account? <a href="<?php echo BASE_URL ?>">Log In</a> </p>
			</form>
		</article>
	</div> <!-- card.// -->
</div> 
<!--container end.//-->
<br><br>

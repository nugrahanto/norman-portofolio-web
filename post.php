<?php 
  include ('header.php'); 

  $posturl      = $_GET['url'];
  $postcategory = $_GET['cat'];

  $postQRY  = "SELECT port_post.*, port_pimg.*  FROM port_post JOIN port_category ON port_post.cat_id = port_category.cat_id JOIN port_pimg ON port_post.post_id = port_pimg.post_id WHERE port_post.post_permalink = :permalink AND port_category.cat_permalink = :catlink AND port_post.status = 'publish' LIMIT 1 ";
  $getPost = $pdo->prepare($postQRY);
  $getPost->bindValue(":permalink", $posturl, PDO::PARAM_STR);
  $getPost->bindValue(":catlink", $postcategory, PDO::PARAM_STR);
  $getPost->execute();
  $postCount = $getPost->rowCount();
  if ($postCount < 1) :
    http_response_code(404);
    include("404.php");
    die();

  else :
    $post    = $getPost->fetchAll(PDO::FETCH_ASSOC);
    $postCID = $post[0]['cat_id'];
?>
    <main class="main main-flush">

      <!-- Post Header
      ================================================== -->
      <div class="post-header">
        <picture class="post-header-picture">
          <source srcset="img/pst/<?php echo $post[0]['img_head1'] ?>?filemtime(img/pst/<?php echo $post[0]['img_head1'] ?>)" media="(min-width: 48em)">
          <source srcset="img/pst/<?php echo $post[0]['img_head3'] ?>?filemtime(img/pst/<?php echo $post[0]['img_head3'] ?>)" media="(min-width: 30em)">
          <source srcset="img/pst/<?php echo $post[0]['img_head2'] ?>?filemtime(img/pst/<?php echo $post[0]['img_head2'] ?>)">
          <img data-object-fit="cover" srcset src="img/pst/<?php echo $post[0]['img_head1'] ?>?filemtime(img/pst/<?php echo $post[0]['img_head1'] ?>)">
        </picture>
      </div>
      <!-- /.post header -->

      <div class="post-wrapper">
        <div class="post-constraint">
          <section class="post-content-wrapper">
            <div class="post-content">

              <div class="post-content-width post-content-picture">
                <div class="post-image">
                  <img src="img/pst/<?php echo $post[0]['img_post'] ?>?filemtime(img/pst/<?php echo $post[0]['img_post'] ?>)">
                </div>
              </div><!-- /.post-content-picture -->

              <div class="post-content-width post-content-desc">
                <article class="post-content-article">
                  <div class="post-content-article-header">
                    <h2><?php echo $post[0]['post_head'] ?></h2>
                  </div><!-- /.post-content-article-header -->
                  <div class="post-content-article-info">
                    <dt>Client</dt>
                    <dd><?php echo $post[0]['post_client'] ?></dd>
                    <dt>Project</dt>
                    <dd><?php echo $post[0]['post_project'] ?></dd>
                    <dt>Location</dt>
                    <dd><?php echo $post[0]['post_location'] ?></dd>
                  </div><!-- /.post-content-article-info -->
                  <div class="post-content-article-detail">
                    <h3>Project Detail</h3>
                    <p><?php echo $post[0]['post_desc'] ?></p>
                  </div><!-- /.post-content-article-detail -->
                  <div class="post-content-article-share">
                    <h3>Share</h3>
                    <!-- I got these buttons from simplesharebuttons.com -->
                    <div class="share-buttons">

                      <!-- Facebook -->
                      <a role="button" target="popup" onclick="window.open('http://www.facebook.com/sharer.php?u=https://simplesharebuttons.com','name','width=600,height=400')">
                        <img class="share-icon" src="assets/icon/fb.png" alt="Facebook" />
                      </a>

                      <!-- Twitter -->
                      <a role="button" target="popup" onclick="window.open('https://twitter.com/share?url=https://simplesharebuttons.com&amp;text=Simple%20Share%20Buttons&amp;hashtags=simplesharebuttons','name','width=600,height=400')">
                        <img class="share-icon" src="assets/icon/tw.png" alt="Twitter" />
                      </a>

                      <!-- LinkedIn -->
                      <a role="button" target="popup" onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url=https://simplesharebuttons.com','name','width=600,height=400')">
                        <img class="share-icon" src="assets/icon/ind.png" alt="LinkedIn" />
                      </a>

                      <!-- Google+ -->
                      <a role="button" target="popup" onclick="window.open('https://plus.google.com/share?url=https://simplesharebuttons.com','name','width=600,height=400')">
                        <img class="share-icon" src="assets/icon/gplus.png" alt="Google" />
                      </a>

                      <!-- Pinterest -->
                      <a role="button" href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">
                        <img class="share-icon" src="assets/icon/pin.png" alt="Pinterest" />
                      </a>

                    </div><!--/.share-button-->
                  </div><!-- /.post-content-article-share -->
                </article>
              </div>
            </div><!-- /.post-content -->
          </section>
        </div>
      </div>

      <section class="more-post-wrapper">
        <div class="more-post-header">
          <h1 class="heading-width more-post-header-heading">More Projects..</h1>
          <p class="heading-width more-post-header-subheading">
            Here is a selection of some other projects from this sections of the portfolio. Please feel free to take a look!
          </p>
        </div>
        <div class="container">

          <!-- START THE FEATURETTES -->
          <div class="row featurette">

          <?php
            $m_postQRY  = "SELECT port_post.post_permalink, port_post.post_head, port_pimg.img_thumb, port_category.cat_name, port_category.cat_permalink FROM port_post JOIN port_category ON port_post.cat_id = port_category.cat_id JOIN port_pimg ON port_post.post_id = port_pimg.post_id WHERE port_category.cat_permalink = :catlink AND port_post.status = :status ORDER BY RAND() LIMIT 6";
            $getPost = $pdo->prepare($m_postQRY);
            $getPost->bindValue(":catlink", $postcategory, PDO::PARAM_STR);
            $getPost->bindValue(":status", 'publish', PDO::PARAM_STR);
            $getPost->execute();
            $getPost = $getPost->fetchAll(PDO::FETCH_ASSOC);

            foreach ($getPost as $result) :
              # code...
          ?>
            <div class="col-md-4" style="padding: 0px;">
              <div class="more-post-content">
                <a href="<?php echo $result['cat_permalink'] ?>/<?php echo $result['post_permalink'] ?>" class="post-thumbnail">
                  <div class="more-post-info">
                    <header>
                      <h2 class="more-post-title"><?php echo $result['post_head'] ?></h2>
                    </header>
                    <p class="more-post-tags"><?php echo $result['cat_name'] ?></p>
                  </div>
                  <img src="img/pst/<?php echo $result['img_thumb'] ?>?filemtime(img/pst/<?php echo $result['img_thumb'] ?>)">
                </a>
              </div>
            </div><!-- /.col-md-4 -->
          <?php endforeach; ?>

          </div><!-- /.row featurette -->
          <!-- /END THE FEATURETTES -->

        </div><!-- /.container -->
      </section>

    </main>

<?php 
  include ('footer.php');
  endif;
?>

<?php
  error_reporting(0);
  session_start();
  include_once("assets/function/koneksi_db.php");
  include("assets/function/userauth_db.php");
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/icon/favicon.png">

    <title>Norman Works</title>

    <!-- Base website -->
    <base href="<?php echo BASE_URL; ?>">

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="assets/css/carousel.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/portfolio.css">

  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div id="bg">
      <img src="assets/img/darkgon.png">
    </div>

    <?php
      $catStmt = $pdo->prepare('SELECT cat_name,cat_permalink,cat_desc,cat_img FROM port_category WHERE status="publish"');
      $catStmt->execute();
      $catFetch = $catStmt->fetchAll(PDO::FETCH_ASSOC);
    ?>

    <div class="navbar-wrapper">
      <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <?php if (isset($_SESSION["userauth-for-admin_token-key"]) AND $_SESSION["userauth-for-admin_token-key"] == 'userauth-ok') : ?>
                <a href="port-admin/" role="button" class="navbar-brand"><p>Hello, <?php echo $_SESSION["access-login"]; ?></p></a>
              <?php else : ?>
                <a role="button" class="navbar-brand">Norman Works</a>
              <?php endif ?>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="<?php echo BASE_URL; ?>">Home</a></li>
                <?php foreach ($catFetch as $cat) : ?>
                  <li><a href="<?php echo $cat['cat_permalink'] ?>"><?php echo $cat['cat_name']; ?></a></li>
                <?php endforeach; ?>
              </ul>
              <ul class="navbar-right"></ul>
              <ul class="nav navbar-nav navbar-right login-form_wrapper">
                <li class="active"><a href="">Contact</a></li>

                <?php if (isset($_SESSION["userauth-for-admin_token-key"]) AND $_SESSION["userauth-for-admin_token-key"] == 'userauth-ok') : ?>
                  <li class="active"><a href="port-admin/" >Dashboard</a></li>
                  <li class="active"><a href="<?php BASE_URL ?>logout.php">logout</a></li>

                <?php else : ?>
                  <li class="active dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">login</a>
                    <ul class="dropdown-menu login-form">
                      <form action="<?php echo BASE_URL; ?>" method="post" class="navbar-form ">
                        <div class="form-group">
                          <li style="font-style: italic; color: #e0707c !important;">
                            <?php 
                              if (isset($_GET['alert'])) {
                                $loginAlert = $_GET['alert'];
                                $loginResult = $_GET['result'];

                                if ($loginAlert == 'login') {
                                  if ($loginResult == 'notfound') {
                                    echo "*) user not found . .";
                                  } elseif ($loginResult == 'invalid') {
                                    echo "*) password incorrect . .";
                                  } elseif ($loginResult == 'notactive') {
                                    echo "*) account still not active . .";
                                  }
                                }
                              }
                            ?>
                          </li>
                        </div>
                        <li class="divider"></li>
                        <div class="form-group">
                          <li>Username / Email</li>
                          <li><input type="text" name="user" class="form-control login-form_margin" value="" placeholder="Username / Email" required></li>
                        </div>
                        <div class="form-group">
                          <li>Password</li>
                          <li><input type="password" name="pass" class="form-control login-form_margin" value="" placeholder="Password" required></li>
                        </div>
                        <li class="divider"></li>
                        <li>
                          <input type="submit" name="user-login" class="login-form__button" value="login">
                          <!--input type="reset" name="user-register" class="login-form__reg-button" value="Register?"-->
                          <a href="user/register" class="login-form__reg-button" style="color: #0cf;">Register?</a>
                        </li>
                      </form>
                    </ul>
                  </li>

                <?php endif ?>

              </ul>
            </div>
          </div>
        </nav>

      </div>
    </div>